/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.desktop.debug;

import com.badlogic.gdx.Preferences;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("LibGDXMissingFlush")
public class PrefixedPreferences implements Preferences {
    private final Preferences preferences;
    private final String prefix;

    public PrefixedPreferences(Preferences preferences, String prefix) {
        this.preferences = preferences;
        this.prefix = prefix + ".";
    }

    @Override
    public Preferences putBoolean(String key, boolean val) {
        preferences.putBoolean(prefix + key, val);
        return this;
    }

    @Override
    public Preferences putInteger(String key, int val) {
        preferences.putInteger(prefix + key, val);
        return this;
    }

    @Override
    public Preferences putLong(String key, long val) {
        preferences.putLong(prefix + key, val);
        return this;
    }

    @Override
    public Preferences putFloat(String key, float val) {
        preferences.putFloat(prefix + key, val);
        return this;
    }

    @Override
    public Preferences putString(String key, String val) {
        preferences.putString(prefix + key, val);
        return this;
    }

    @Override
    public Preferences put(Map<String, ?> vals) {
        final Map<String, Object> prefixedMap = new HashMap<>();
        for (Map.Entry<String, ?> val : vals.entrySet()) {
            prefixedMap.put(prefix + val.getKey(), val.getValue());
        }
        preferences.put(prefixedMap);
        return this;
    }

    @Override
    public boolean getBoolean(String key) {
        return preferences.getBoolean(prefix + key);
    }

    @Override
    public int getInteger(String key) {
        return preferences.getInteger(prefix + key);
    }

    @Override
    public long getLong(String key) {
        return preferences.getLong(prefix + key);
    }

    @Override
    public float getFloat(String key) {
        return preferences.getFloat(prefix + key);
    }

    @Override
    public String getString(String key) {
        return preferences.getString(prefix + key);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(prefix + key, defValue);
    }

    @Override
    public int getInteger(String key, int defValue) {
        return preferences.getInteger(prefix + key, defValue);
    }

    @Override
    public long getLong(String key, long defValue) {
        return preferences.getLong(prefix + key, defValue);
    }

    @Override
    public float getFloat(String key, float defValue) {
        return preferences.getFloat(prefix + key, defValue);
    }

    @Override
    public String getString(String key, String defValue) {
        return preferences.getString(prefix + key, defValue);
    }

    @Override
    public Map<String, ?> get() {
        final Map<String, Object> map = new HashMap<>();
        int prefixLen = prefix.length();
        for (Map.Entry<String, ?> val : preferences.get().entrySet()) {
            map.put(val.getKey().substring(prefixLen), val.getValue());
        }
        return map;
    }

    @Override
    public boolean contains(String key) {
        return preferences.contains(prefix + key);
    }

    @Override
    public void clear() {
        preferences.clear();
    }

    @Override
    public void remove(String key) {
        preferences.remove(prefix + key);
    }

    @Override
    public void flush() {
        preferences.flush();
    }
}
