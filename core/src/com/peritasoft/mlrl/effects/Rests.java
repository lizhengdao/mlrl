/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.effects;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;


public class Rests extends LifeTurnObj {
    private final Character character;
    private final Level level;

    public Rests(Position position, int infinite) {
        this(position, infinite, null, null);
    }

    public Rests(Position position, int liveTurns, Character character, Level level) {
        super(position, liveTurns, LifeObjLayer.UNDER, LifeObjType.BONES);
        this.character = character;
        this.level = level;
    }

    @Override
    public void update(boolean nextTurn) {
        super.update(nextTurn);
        if (nextTurn && !isAlive() && character != null) {
            if (!character.raise(level)) {
                this.incTurns(1);
            }
        }
    }
}
