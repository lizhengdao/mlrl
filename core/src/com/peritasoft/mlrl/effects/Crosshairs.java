/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.effects;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Geometry;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.weapons.Shootable;

public class Crosshairs {
    private final Position initialPos;
    private final Position position;
    private final Shootable shootable;

    public Crosshairs(final Position pos, final Direction dir, Shootable shootable) {
        this.initialPos = pos;
        this.position = new Position(pos.getX() + dir.x, pos.getY() + dir.y);
        this.shootable = shootable;
    }

    public Position getPosition() {
        return position;
    }

    public Projectile shoot(Character shooter, Level level) {
        ProjectileType projectileType = shootable.shoot(shooter, position, level);
        if (projectileType == null) {
            return null;
        }
        return new Projectile(position, shooter, projectileType, shootable);
    }

    public void move(Direction dir, Level level) {
        Position newPos = new Position(position.getX() + dir.x, position.getY() + dir.y);
        int distance = initialPos.distance(newPos);
        if (distance > shootable.getRange()) return;
        boolean walkable = level.hasNoObstacles(initialPos, newPos);
        if (walkable) position.set(newPos);
    }

    public void moveTo(Position position, final Level level) {
        this.position.set(initialPos);
        Geometry.walkLine(initialPos, position, new Geometry.LineWalker() {
            @Override
            public boolean walk(Position p) {
                if (level.getCell(p.getX(), p.getY()).blocksSight()) return false;
                if (initialPos.distance(p) > shootable.getRange()) return false;
                Crosshairs.this.position.set(p);
                return true;
            }
        });
    }

    public boolean isAtInitialPosition() {
        return position.equals(initialPos);
    }
}
