/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.characters.Demography;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.characters.Sex;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.screens.SelectScreen;
import com.peritasoft.mlrl.ui.Button;
import com.peritasoft.mlrl.ui.FixedSizeButton;

import java.util.Set;

public class SelectRenderer implements Disposable {
    private static final float CHOICE_SEPARATION = 316f;
    public static final float BOTTOM_REMARK_WIDTH = 115f;

    private final RendererAtlas atlas;
    private final BitmapFont textFont;
    private final NinePatch inventoryBackground;
    private final TextureAtlas atlasInterface;
    private final PortraitMap portraits;
    private final Rectangle choiceRect;
    private final Button rerollButton;
    private final Button femaleButton;
    private final Button maleButton;
    private final FixedSizeButton nextButton;
    private final FixedSizeButton prevButton;
    private final NinePatch listBackground;
    private float scrollOffset;

    public SelectRenderer(BitmapFont textFont, int initialSelection) {
        this.textFont = textFont;
        this.choiceRect = new Rectangle(
                MyLittleRogueLike.SCREENWIDTH / 2f - 280f / 2f,
                MyLittleRogueLike.SCREENHEIGHT / 2f - 130f / 2f - 20f,
                280f,
                130f
        );
        atlas = new PrettyRendererAtlas();
        atlasInterface = new TextureAtlas("td_interface.atlas");
        inventoryBackground = new NinePatch(atlasInterface.findRegion("backgroundInventory"), 16, 16, 16, 16);
        listBackground = new NinePatch(atlasInterface.findRegion("backgroundList"), 10, 10, 5, 5);
        final TextureAtlas.AtlasRegion chevron = atlasInterface.findRegion("buttonChevron");
        this.prevButton = new FixedSizeButton(choiceRect.x - 30f, 90, chevron, 90);
        this.nextButton = new FixedSizeButton(choiceRect.x + choiceRect.width + 30f - 16f, 90, chevron, 270);
        final NinePatch emptyButton = new NinePatch(atlasInterface.findRegion("buttonEmpty"), 8, 8, 4, 4);
        final NinePatch emptyButtonPressed = new NinePatch(atlasInterface.findRegion("buttonEmptyPressed"), 8, 8, 4, 4);
        this.rerollButton = new Button(
                MyLittleRogueLike.SCREENWIDTH / 2f - 64f,
                MyLittleRogueLike.SCREENHEIGHT / 6f - 12f - 24f,
                128,
                emptyButton,
                "Reroll Items"
        );
        this.femaleButton = new Button(
                MyLittleRogueLike.SCREENWIDTH / 2f,
                MyLittleRogueLike.SCREENHEIGHT - 60f,
                128,
                emptyButton,
                emptyButtonPressed,
                "Female"
        );
        this.maleButton = new Button(
                MyLittleRogueLike.SCREENWIDTH / 2f - 128f,
                MyLittleRogueLike.SCREENHEIGHT - 60f,
                128,
                emptyButton,
                emptyButtonPressed,
                "Male"
        );
        portraits = new PortraitMap();
        this.scrollOffset = getSelectionOffset(initialSelection);
    }

    public void render(Batch batch, SelectScreen.Choice[] choices, Set<Demography> unlockedChoices, Sex sex, int selected, float globalAlpha) {
        final float wantedOffset = getSelectionOffset(selected);
        final float speed = 1024f;
        if (scrollOffset < wantedOffset) {
            scrollOffset = Math.min(scrollOffset + speed * Gdx.graphics.getDeltaTime(), wantedOffset);
        } else if (scrollOffset > wantedOffset) {
            scrollOffset = Math.max(scrollOffset - speed * Gdx.graphics.getDeltaTime(), wantedOffset);
        }
        for (int c = 0; c < choices.length; c++) {
            final float offsetX = getSelectionOffset(c) - scrollOffset;
            final SelectScreen.Choice choice = choices[c];
            final boolean unlocked = unlockedChoices.contains(choice.getDemography());
            final float alpha = (unlocked ? 1f : 0.5f) * globalAlpha;
            batch.setColor(1f, 1f, 1f, alpha);
            renderChoice(batch, choice.getPlayer(), offsetX, globalAlpha);
            renderInventory(batch, choice.getInventory(), offsetX, alpha);
            batch.setColor(1f, 1f, 1f, globalAlpha);
            if (unlocked) {
                renderScore(batch, choice.getMaxFloor(), choice.getKillerName(), offsetX);
            } else {
                renderUnlockCondition(batch, choice.getDemography(), offsetX, globalAlpha);
            }
        }
        textFont.setColor(1f, 1f, 1f, globalAlpha);
        rerollButton.draw(batch, textFont);
        femaleButton.draw(batch, textFont, sex == Sex.FEMALE);
        maleButton.draw(batch, textFont, sex == Sex.MALE);
        if (selected > 0) {
            prevButton.draw(batch);
        }
        if (selected < choices.length - 1) {
            nextButton.draw(batch);
        }
    }

    private static float getSelectionOffset(int selected) {
        return CHOICE_SEPARATION * (selected - 1);
    }

    private void renderScore(Batch batch, int maxFloor, String killerName, float offsetX) {
        final float x = choiceRect.x + offsetX + 96f + 48f;
        final float y = choiceRect.y + choiceRect.height - 32f - 40f + 6f - 12f * 2;
        if (maxFloor < 0) {
            textFont.draw(batch, "No quest finished yet", x, y + 12, BOTTOM_REMARK_WIDTH, Align.left, true);
        } else {
            textFont.draw(batch, "Reached floor " + maxFloor, x, y + 12);
            textFont.draw(batch, killerName.isEmpty() ? "Game Complete" : "Killed by " + killerName, x, y, BOTTOM_REMARK_WIDTH, Align.left, true);
        }
    }

    private void renderUnlockCondition(Batch batch, Demography demography, float offsetX, float alpha) {
        Demography boss;
        switch (demography) {
            case HERO_ARCHER:
                boss = Demography.TRASGA_HAMELIN;
                break;
            case HERO_MAGE:
                boss = Demography.URMUK;
                break;
            default:
                throw new IllegalArgumentException();
        }
        final String condition = "Defeat " + boss.name + " to unlock this character";
        final float x = choiceRect.x + offsetX + 96f + 48f;
        final float y = choiceRect.y + choiceRect.height - 32f - 40f - 12f + 6f;
        textFont.setColor(1f, 0.9f, 0f, alpha);
        textFont.draw(batch, condition, x, y, BOTTOM_REMARK_WIDTH, Align.left, true);
        textFont.setColor(0f, 0f, 0f, alpha);
    }


    private void renderChoice(Batch batch, PlayerHero player, float offsetX, float alpha) {
        inventoryBackground.draw(batch, choiceRect.x + offsetX, choiceRect.y, choiceRect.width, choiceRect.height);

        TextureAtlas.AtlasRegion portrait = portraits.get(player.getSex(), player.getDemography());
        float x = choiceRect.x + portrait.getRegionWidth() / 2f + offsetX + 40f;
        float y = choiceRect.y + choiceRect.height - portrait.getRegionHeight() - 4f;
        batch.draw(portrait, x, y);

        float labelX = choiceRect.x + offsetX + 60f;
        float labelY = choiceRect.y + choiceRect.height - portrait.getRegionHeight() - 40f + 6f;
        float labelH = 12f;

        textFont.setColor(0f, 0f, 0f, 0.85f * alpha);
        drawStat(batch, labelX, labelY - labelH * 0, "Strength:", player.getStr());
        drawStat(batch, labelX, labelY - labelH * 1, "Dextrery:", player.getDex());
        drawStat(batch, labelX, labelY - labelH * 2, "Wisdom:", player.getWis());
        drawStat(batch, labelX, labelY - labelH * 3, "Constitution:", player.getCon());
        textFont.draw(batch, player.getName(), x, y - 2f, portrait.getRegionWidth(), Align.center, false);
        textFont.draw(batch, player.getDemography().klass.name().toLowerCase(), x, y - 13f, portrait.getRegionWidth(), Align.center, false);
    }

    private void drawStat(Batch batch, float labelX, float labelY, String statName, int stat) {
        float labelW = 57f;
        float spacing = 4f;
        textFont.draw(batch, statName, labelX, labelY, labelW, Align.right, false);
        textFont.draw(batch, String.valueOf(stat), labelX + labelW + spacing, labelY);
    }

    private void renderInventory(Batch batch, Inventory inventory, float offsetX, float alpha) {
        final float y = choiceRect.y + choiceRect.height - 42f;
        for (int i = 0; i < 3; i++) {
            final float x = choiceRect.x + 96f + 48f + 35f * i;
            renderItem(batch, inventory.get(i), x, y, offsetX, alpha);
            renderItem(batch, inventory.size() > (i + 3) ? inventory.get(i + 3) : null, x, y - 34f, offsetX, alpha);
        }
    }

    private void renderItem(Batch batch, Item item, float x, float y, float offsetX, float alpha) {
        listBackground.draw(batch, x + offsetX, y, 32f, 32f);
        if (item == null) return;
        Sprite sprite = atlas.getItem(item.getCategory());
        sprite.setAlpha(alpha);
        sprite.setPosition(x + 16f - sprite.getWidth() / 2f + offsetX, y + 16f - sprite.getHeight() / 2f);
        sprite.draw(batch);
        sprite.setAlpha(1f);
    }

    @Override
    public void dispose() {
        atlas.dispose();
        portraits.dispose();
        atlasInterface.dispose();
    }

    public boolean nextClicked(Vector2 touchPoint) {
        return nextButton.pressed(touchPoint);
    }

    public boolean prevClicked(Vector2 touchPoint) {
        return prevButton.pressed(touchPoint);
    }

    public boolean rerollClicked(Vector2 touchPoint) {
        return rerollButton.pressed(touchPoint);
    }

    public boolean femaleClicked(Vector2 touchPoint) {
        return femaleButton.pressed(touchPoint);
    }

    public boolean maleClicked(Vector2 touchPoint) {
        return maleButton.pressed(touchPoint);
    }

    public boolean choiceClicked(Vector2 touchPoint) {
        return choiceRect.contains(touchPoint);
    }
}
