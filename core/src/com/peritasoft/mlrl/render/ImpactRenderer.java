/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class ImpactRenderer {
    private final float FRAME_DURATION = 0.05f;
    private Animation<Sprite> animation;

    ImpactRenderer(TextureAtlas atlas, String kind) {
        animation = new Animation<>(FRAME_DURATION,
                TextureAtlasHelper.mustCreateSprite(atlas, kind, 1),
                TextureAtlasHelper.mustCreateSprite(atlas, kind, 2),
                TextureAtlasHelper.mustCreateSprite(atlas, kind, 3)
        );
    }

    void render(SpriteBatch batch, int x, int y, float timer) {
        Sprite sprite = animation.getKeyFrame(timer, false);
        sprite.setPosition(x, y);
        sprite.draw(batch);
    }
}
