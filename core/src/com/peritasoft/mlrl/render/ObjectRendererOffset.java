/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

class ObjectRendererOffset extends ObjectRenderer {
    int offsetX;
    int offsetY;

    ObjectRendererOffset(TextureAtlas atlas, String kind, int variations) {
        super(atlas, kind, variations);
        offsetX = 0;
        offsetY = 0;
    }

    ObjectRendererOffset(TextureAtlas atlas, String kind, int variations, int offsetX, int offsetY) {
        super(atlas, kind, variations);
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }

    public void setOffsetY(int offsetY) {
        this.offsetY = offsetY;
    }

    void render(SpriteBatch batch, int x, int y, int variation, float alpha) {
        Sprite sprite = regions[variation % regions.length];
        sprite.setPosition(x + offsetX, y + offsetY);
        sprite.draw(batch, alpha);
    }
}
