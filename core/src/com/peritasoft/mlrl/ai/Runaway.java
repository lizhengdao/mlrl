/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;

public class Runaway extends Behaviour {
    @Override
    public Action update(Level level, Character me, Character enemy) {
        Direction nextDir = Direction.NONE;
        if (enemy != null) {
            Position enemyPos = enemy.getPrevPosition();
            switch (me.getPosition().directionTo(enemyPos)) {
                case NORTH:
                    nextDir = chooseNextDirection(me, level, Direction.SOUTH, Direction.WEST, Direction.EAST);
                    break;
                case SOUTH:
                    nextDir = chooseNextDirection(me, level, Direction.NORTH, Direction.WEST, Direction.EAST);
                    break;
                case WEST:
                    nextDir = chooseNextDirection(me, level, Direction.EAST, Direction.NORTH, Direction.SOUTH);
                    break;
                case EAST:
                    nextDir = chooseNextDirection(me, level, Direction.WEST, Direction.NORTH, Direction.SOUTH);
                    break;
                default:
                    nextDir = Direction.NONE;
            }
        }
        return me.move(nextDir, level);
    }

    private Direction chooseNextDirection(Character me, Level level, Direction primary, Direction sideA, Direction sideB) {
        if (canMoveTo(level, me.getPosition(), primary)) return primary;
        boolean aComesFirst = MathUtils.randomBoolean();
        final Direction firstSide = aComesFirst ? sideA : sideB;
        final Direction lastSide = aComesFirst ? sideB : sideA;
        if (canMoveTo(level, me.getPosition(), firstSide)) return firstSide;
        if (canMoveTo(level, me.getPosition(), lastSide)) return lastSide;
        return Direction.NONE;
    }

    private boolean canMoveTo(Level level, Position position, Direction direction) {
        final int x = position.getX() + direction.x;
        final int y = position.getY() + direction.y;
        return level.isWalkable(x, y) && !level.hasCharacterIn(x, y);
    }
}
