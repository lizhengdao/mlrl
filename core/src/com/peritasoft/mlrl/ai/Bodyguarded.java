/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.*;

import java.util.ArrayList;

public class Bodyguarded extends Behaviour {
    private final ArrayList<Character> bodyguards;
    private Character victim;
    private final DijkstraMapBuilder mapBuilder;
    private final DijkstraMap fleeMap;
    private final DijkstraMap safetyMap;
    private final Behaviour next;

    public Bodyguarded() {
        this.bodyguards = new ArrayList<>();
        mapBuilder = new DijkstraMapBuilder();
        fleeMap = new DijkstraMap();
        safetyMap = new DijkstraMap();
        next = new WanderSeekApproach(false);
    }

    @Override
    public Action update(Level level, Character me, Character enemy) {
        if (bodyguards.isEmpty()) {
            findBodyguards(level, me);
        }
        if (areAllBodyguardsDead()) {
            return next.update(level, me, enemy);
        }
        if (victim == null) {
            // Enemy does not work because it is only within the radius range
            // and we want the search enemy in the *whole* level.
            victim = findVictim(level, me);
            if (victim == null) {
                return Action.NONE;
            }
        }
        computeFleeMap(level, victim);
        computeSafeMap(level);
        Direction nextDir = Direction.NONE;
        float min = Float.MAX_VALUE;
        for (Direction dir : Direction.values()) {
            int row = me.getPositionY() + dir.y;
            int col = me.getPositionX() + dir.x;
            if (level.isWalkable(col, row)) {
                float v = fleeMap.getDistance(row, col) * -1.2f + safetyMap.getDistance(row, col) * 10f;
                if (v < min) {
                    min = v;
                    nextDir = dir;
                }
            }
        }
        return me.move(nextDir, level);
    }

    private void computeFleeMap(Level level, Character victim) {
        mapBuilder.reset(level, fleeMap);
        mapBuilder.addGoal(victim.getPositionY(), victim.getPositionX());
        mapBuilder.build();
    }

    private void computeSafeMap(Level level) {
        mapBuilder.reset(level, safetyMap);
        for (Character c : bodyguards) {
            if (c.isDead()) continue;
            final int radius = 2;
            int minCol = c.getPositionX() - radius;
            int maxCol = c.getPositionX() + radius;
            int minRow = c.getPositionY() - radius;
            int maxRow = c.getPositionY() + radius;
            for (int col = minCol; col <= maxCol; col++) {
                mapBuilder.addGoal(minRow, col);
                mapBuilder.addGoal(maxRow, col);
            }
            for (int row = minRow; row <= maxRow; row++) {
                mapBuilder.addGoal(row, minCol);
                mapBuilder.addGoal(row, maxCol);
            }
        }
        mapBuilder.build();
    }

    private void findBodyguards(Level level, Character me) {
        for (int row = 0; row <= level.getHeight(); row++) {
            for (int col = 0; col <= level.getWidth(); col++) {
                Cell cell = level.getCell(col, row);
                final Character character = cell.getCharacter();
                if (character != null && character != me && !me.canAttack(character)) {
                    bodyguards.add(character);
                }
            }
        }
    }

    private Character findVictim(Level level, Character me) {
        for (int row = 0; row <= level.getHeight(); row++) {
            for (int col = 0; col <= level.getWidth(); col++) {
                Cell cell = level.getCell(col, row);
                final Character character = cell.getCharacter();
                if (character != null && me.canAttack(character)) {
                    return character;
                }
            }
        }
        return null;
    }

    private boolean areAllBodyguardsDead() {
        for (Character b : bodyguards) {
            if (!b.isDead()) {
                return false;
            }
        }
        return true;
    }

}
