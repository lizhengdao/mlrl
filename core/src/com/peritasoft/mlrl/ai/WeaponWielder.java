/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.KindOfWeapon;

public class WeaponWielder extends Behaviour {
    Behaviour defaultBehaviour;

    public WeaponWielder(Behaviour defaultBehaviour) {
        this.defaultBehaviour = defaultBehaviour;
    }

    @Override
    public Action update(Level level, Character me, Character enemy) {
        int weaponIdx = hasBetterWeapon(me);
        if (weaponIdx > -1) {
            Item weapon = me.getInventory().get(weaponIdx);
            me.equip(weapon);
            GameEvent.equipWeapon(me);
            return Action.EQUIP_ITEM;
        }
        return defaultBehaviour.update(level, me, enemy);
    }

    private int hasBetterWeapon(Character c) {
        KindOfWeapon equippedWeapon = c.getEquippedWeapon();
        for (int j = 0; j < c.getInventory().size(); j++) {
            Item i = c.getInventory().get(j);
            if (i instanceof KindOfWeapon) {
                KindOfWeapon w = (KindOfWeapon) i;
                if (w.getQualityIndex(c) > equippedWeapon.getQualityIndex(c)) {
                    return j;
                }
            }
        }
        return -1;
    }
}
