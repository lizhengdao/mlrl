/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.DarkKnightShadow;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;

public class DarkKnightBehaviour extends Behaviour {
    private final Behaviour normalBehaviour;
    private boolean duplicated;

    public DarkKnightBehaviour(Behaviour normalBehaviour) {
        this.normalBehaviour = normalBehaviour;
        this.duplicated = false;
    }

    @Override
    public Action update(Level level, Character me, Character enemy) {
        if (!duplicated && me.getHp() <= me.getMaxHp() / 2) {
            duplicated = duplicate(level, me);
            if (duplicated) return Action.NONE;
        }
        return normalBehaviour.update(level, me, enemy);
    }

    protected boolean duplicate(Level level, Character me) {
        final int radius = 1;
        final int minY = me.getPositionY() - radius;
        final int maxY = me.getPositionY() + radius;
        final int minX = me.getPositionX() - radius;
        final int maxX = me.getPositionX() + radius;
        for (int y = minY; y <= maxY; y++) {
            for (int x = minX; x <= maxX; x++) {
                final Cell cell = level.getCell(x, y);
                if (!cell.isWalkable() || cell.hasCharacter()) {
                    continue;
                }
                Position shadowPosition = new Position(x, y);
                Direction shadowDirection = shadowPosition.directionTo(me.getPrevPosition());
                if (MathUtils.randomBoolean()) {
                    final Position myPosition = new Position(me.getPosition());
                    me.setPosition(shadowPosition, level);
                    shadowPosition.set(myPosition);
                    final Direction myDirection = me.getLastDirection();
                    me.setLastDirection(shadowDirection);
                    shadowDirection = myDirection;
                }
                DarkKnightShadow s = new DarkKnightShadow(me.getLevel(), shadowPosition);
                s.setLastDirection(shadowDirection);
                level.addEnemy(s);
                return true;
            }
        }
        return false;
    }
}
