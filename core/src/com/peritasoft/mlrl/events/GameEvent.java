/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.events;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.ItemCategory;
import com.peritasoft.mlrl.item.Potion;

import java.util.ArrayList;
import java.util.List;

public class GameEvent {
    final private static List<GameEventListener> listeners = new ArrayList<>();

    private GameEvent() {
        // Nothing to do; only used to mark this class as “static”
    }

    public static void register(GameEventListener listener) {
        if (listener == null) {
            return;
        }
        if (listeners.contains(listener)) {
            return;
        }
        listeners.add(listener);
    }

    public static void unregister(GameEventListener listener) {
        listeners.remove(listener);
    }

    public static void attackHit(Character attacker, Character victim, int damage, ItemCategory itemCategory) {
        for (final GameEventListener listener : listeners) {
            listener.attackHit(attacker, victim, damage, itemCategory);
        }
    }

    public static void attackMissed(Character attacker, Character victim) {
        for (final GameEventListener listener : listeners) {
            listener.attackMissed(attacker, victim);
        }
    }

    public static void died(PlayerHero player) {
        for (final GameEventListener listener : listeners) {
            listener.died(player);
        }
    }

    public static void drank(Potion potion, Character drinker) {
        for (final GameEventListener listener : listeners) {
            listener.drank(potion, drinker);
        }
    }

    public static void pickedUp(Character character, Item item) {
        for (final GameEventListener listener : listeners) {
            listener.pickedUp(character, item);
        }
    }

    public static void killed(Character c) {
        for (final GameEventListener listener : listeners) {
            listener.killed(c);
        }
    }

    public static void lookedAt(Item item) {
        for (final GameEventListener listener : listeners) {
            listener.lookedAt(item);
        }
    }

    public static void luredByDecoy(Character enemy) {
        for (final GameEventListener listener : listeners) {
            listener.luredByDecoy(enemy);
        }
    }

    public static void openedChest(Item item) {
        for (final GameEventListener listener : listeners) {
            listener.openedChest(item);
        }
    }

    public static void spottedPlayer(Character enemy) {
        for (final GameEventListener listener : listeners) {
            listener.spottedPlayer(enemy);
        }
    }

    public static void fireSpellgemCracked() {
        for (final GameEventListener listener : listeners) {
            listener.fireSpellgemCracked();
        }
    }

    public static void levelUp(int newLevel, int strUp, int dexUp, int wisUp, int conUp) {
        for (final GameEventListener listener : listeners) {
            listener.levelUp(newLevel, strUp, dexUp, wisUp, conUp);
        }
    }

    public static void teleported() {
        for (final GameEventListener listener : listeners) {
            listener.teleported();
        }
    }

    public static void teleported(Character me) {
        for (final GameEventListener listener : listeners) {
            listener.teleported(me);
        }
    }

    public static void healed(Character character, int hpHealed) {
        for (final GameEventListener listener : listeners) {
            listener.healed(character, hpHealed);
        }
    }

    public static void castFireNova() {
        for (final GameEventListener listener : listeners) {
            listener.castFireNova();
        }
    }

    public static void castIceNova() {
        for (final GameEventListener listener : listeners) {
            listener.castIceNova();
        }
    }

    public static void newGame(long seed) {
        for (final GameEventListener listener : listeners) {
            listener.newGame(seed);
        }
    }

    public static void pauseGame() {
        for (final GameEventListener listener : listeners) {
            listener.pauseGame();
        }
    }

    public static void resumeGame(long seed) {
        for (final GameEventListener listener : listeners) {
            listener.resumeGame(seed);
        }
    }

    public static void petrified(Character character, int turns) {
        for (final GameEventListener listener : listeners) {
            listener.petrified(character, turns);
        }
    }

    public static void confused(Character character) {
        for (final GameEventListener listener : listeners) {
            listener.confused(character);
        }
    }

    public static void skipPetrifiedTurn(Character character, int turns) {
        for (final GameEventListener listener : listeners) {
            listener.skipPetrifiedTurn(character, turns);
        }
    }

    public static void skipFrozenTurn(Character character, int turns) {
        for (final GameEventListener listener : listeners) {
            listener.skipFrozenTurn(character, turns);
        }
    }

    public static void notEnoughWis() {
        for (final GameEventListener listener : listeners) {
            listener.notEnoughWis();
        }
    }

    public static void notEnoughMana(Character character) {
        for (final GameEventListener listener : listeners) {
            listener.notEnoughMana(character);
        }
    }

    public static void attackByPoison(Character attacker, Character victim, int damage) {
        for (final GameEventListener listener : listeners) {
            listener.attackByPoison(attacker, victim, damage);
        }
    }

    public static void shootMissed() {
        for (final GameEventListener listener : listeners) {
            listener.shootMissed();
        }
    }

    public static void openedEmptyChest() {
        for (final GameEventListener listener : listeners) {
            listener.openedEmptyChest();
        }
    }

    public static void raiseStat(Character character, String stat, int amountRaised) {
        for (final GameEventListener listener : listeners) {
            listener.raiseStat(character, stat, amountRaised);
        }
    }

    public static void changedFloor(int floor, Level level) {
        for (final GameEventListener listener : listeners) {
            listener.changedFloor(floor, level);
        }
    }

    public static void inventoryFull() {
        for (final GameEventListener listener : listeners) {
            listener.inventoryFull();
        }
    }

    public static void weaponEmbedded(Character attacker) {
        for (final GameEventListener listener : listeners) {
            listener.weaponEmbedded(attacker);
        }
    }

    public static void summonMob(Character summoner, Character summoned) {
        for (final GameEventListener listener : listeners) {
            listener.summonMob(summoner, summoned);
        }
    }

    public static void raised(Character character) {
        for (final GameEventListener listener : listeners) {
            listener.raised(character);
        }
    }


    public static void intangibleTarget(Character character) {
        for (final GameEventListener listener : listeners) {
            listener.intangibleTarget(character);
        }
    }

    public static void canNotShootSelf(Character character) {
        for (final GameEventListener listener : listeners) {
            listener.canNotShootSelf(character);
        }
    }

    public static void inspectBookshelf(Item item) {
        for (final GameEventListener listener : listeners) {
            listener.inspectBookshelf(item);
        }
    }

    public static void equipWeapon(Character character) {
        for (final GameEventListener listener : listeners) {
            listener.equipWeapon(character);
        }
    }

    public static void playerMoves() {
        for (final GameEventListener listener : listeners) {
            listener.playerMoves();
        }
    }

    public static void openInventory() {
        for (final GameEventListener listener : listeners) {
            listener.openInventory();
        }
    }

    public static void closeInventory() {
        for (final GameEventListener listener : listeners) {
            listener.closeInventory();
        }
    }

    public static void poison() {
        for (final GameEventListener listener : listeners) {
            listener.poison();
        }
    }

    public static void castLightningStorm() {
        for (final GameEventListener listener : listeners) {
            listener.castLightningStorm();
        }
    }

    public static void dispelledEffects(Character character) {
        for (final GameEventListener listener : listeners) {
            listener.dispelledEffects(character);
        }
    }

    public static void foundCoins() {
        for (final GameEventListener listener : listeners) {
            listener.foundCoins();
        }
    }

    public static void attackByFire(Character attacker, Character victim, int damage) {
        for (final GameEventListener listener : listeners) {
            listener.attackByFire(attacker, victim, damage);
        }
    }

    public static void castFireWall() {
        for (final GameEventListener listener : listeners) {
            listener.castFireWall();
        }
    }

    public static void endGame(PlayerHero player) {
        for (final GameEventListener listener : listeners) {
            listener.endGame(player);
        }
    }

    public static void deathChangedPersona() {
        for (final GameEventListener listener : listeners) {
            listener.deathChangedPersona();
        }
    }
}
