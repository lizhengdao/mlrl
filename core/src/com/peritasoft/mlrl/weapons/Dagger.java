/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.ItemCategory;

public class Dagger extends MeleeWeapon implements Weapon {

    public Dagger(int bonusStr, int bonusDex, int bonusCon, int bonusWis, int bonusDamage) {
        super("Dagger", "A simply dagger", bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
        this.description = getRandomDescription();
    }

    @Override
    public Dagger copy() {
        return new Dagger(getBonusStr(), getBonusDex(), getBonusCon(), getBonusWis(), getBonusDamage());
    }

    @Override
    public void attack(Character attacker, Character target, Level level) {
        if (MathUtils.random(1, 20) + attacker.getDex() > MathUtils.random(1, 20) + target.getDex()) {
            int dmg = (attacker.getStr() / 2) + rollBonusDamage();
            GameEvent.attackHit(attacker, target, dmg, getCategory());
            target.receiveHit(dmg, attacker);
        } else {
            GameEvent.attackMissed(attacker, target);
        }
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.WEAPON_DAGGER;
    }

    private String getRandomDescription() {
        String[] desc = {"This dagger has a long double-edged blade forged from silver.",
                "The blade is fixed on a rich hilt of shinning gold metal with a smooth grip made of bluish crystal.",
                "A set of strange runes (a spear plunged into a fleur-de-lys atop a dragon) are carved into the blade near the spine.",
                "The dagger has an ornate blade in serpentine shape forged in grayish steel.",
                "The blade is fixed on a rich dark metal hilt . The blade appears brand new.",
                "This dagger has a long double-edged blade forged from grayish steel.",
                "The blade is fixed on a beautiful hilt sculpted in the shape of ape inlayed with gold and platinum.",
                "A philosophical phrase (A pledge of poverty) in ancient runes is carved into the blade. The blade is in excellent shape.",
                "The dagger has a short double-edged blade forged in lead.",
                "The blade is fixed on a rich hilt of shinning gold metal covered in demon hide.",
                "A motto in ornate writing (undercommon) is carved into the blade. The blade is in excellent shape.",
                "The dagger has an ornate blade in serpentine shape made from copper (red).",
                "The blade is fixed to a hilt covered in ordinary leather.",
                "This dagger has a short double-edged blade forged in brass.",
                "The blade is fixed to a massive hilt.",
                "A set of strange runes (maple tree-shaped) are carved into the blade near the back.",
                "This dagger is a long knife with a sturdy and stocky one-edged blade forged from solid iron.",
                "The blade is fixed to a beautiful hilt sculpted in the shape of kraken with a spiral-turned grip made of maple wood.",
                "Abstract patterns are carved along the blade. The blade has been chipped and scratched.",
                "This dagger is a short but heavy dagger with a curved blade sharpened only on the outer part forged in black steel.",
                "The blade is fixed to a rich smoke metal hilt encrusted with precious gems of various colors.",
                "The weapon’s name (Graveth) in ornate writing is carved into the blade. The blade is rusty.",
                "The dagger has a long double-edged blade forged from black steel.",
                "The blade is fixed on a rich hilt of ebony metal covered in red leather. The blade is in excellent shape.",
                "The dagger is a long and rough knife forged in an alloy of steel and silver.",
                "The dagger also has a straight silvery hilt covered in black dragon hide. Scenes of battle are carved into the blade."};
        return desc[MathUtils.random(desc.length - 1)];
    }
}
