/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.BurnTurns;
import com.peritasoft.mlrl.events.GameEvent;

public class FireWall {
    final int length;
    final int turns;
    final int dmg;

    public FireWall(int length, int turns, int dmg) {
        this.length = length;
        this.turns = turns;
        this.dmg = dmg;
    }

    public void cast(Character attacker, Level level, Position initialPos, Direction dir) {
        GameEvent.castFireWall();
        Position next = new Position(initialPos);
        for (int i = 0; i < length; i++) {
            Cell cell = level.getCell(next.getX(), next.getY());
            if (!cell.isWalkable()) {
                continue;
            }
            level.getLifeObjs().add(new BurnTurns(dmg, level, next, attacker, turns));

            next = new Position(next.getX() + dir.x, next.getY() + dir.y);
        }
    }
}
