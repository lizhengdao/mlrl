/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.Burn;
import com.peritasoft.mlrl.effects.LifeObjType;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.ItemCategory;

public class LightningStorm {
    final int radius;
    final int dmg;
    final ItemCategory itemCategory;

    public LightningStorm(int radius, int dmg, ItemCategory itemCategory) {
        this.radius = radius;
        this.dmg = dmg;
        this.itemCategory = itemCategory;
    }

    public void cast(Character attacker, Level level, Position pos) {
        GameEvent.castLightningStorm();
        for (int y = -radius; y <= radius; y++) {
            for (int x = -radius; x <= radius; x++) {
                if ((x == 0 || y == 0) && (x != 0 || y != 0))  continue;
                Position position = new Position(pos.getX() + x, pos.getY() + y);
                Cell cell = level.getCell(position.getX(), position.getY());
                if (!cell.isWalkable()) {
                    continue;
                }
                level.getLifeObjs().add(new Burn(position, LifeObjType.LIGHTNING));
                Character e = cell.getCharacter();
                if (e != null) {
                    int damage = e == attacker ? dmg/2 : dmg;
                    GameEvent.attackHit(attacker, e, damage, itemCategory);
                    e.receiveHit(damage, attacker);
                    if (MathUtils.random() <= 0.8f) e.confuse();
                }
            }
        }
    }
}
