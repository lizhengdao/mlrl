/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.PotionColour;

import java.util.ArrayList;

public class PlayerHeroSerializer extends CharacterSerializer<PlayerHero> {
    private PlayerHero playerHero;

    @Override
    protected void writeOwnProperties(Json json, PlayerHero object) {
        json.writeValue(object.getDemography());
        json.writeValue(object.getSex());
        json.writeValue(object.xp);
        json.writeValue(object.getStr());
        json.writeValue(object.getDex());
        json.writeValue(object.getWis());
        json.writeValue(object.getCon());
        json.writeArrayStart();
        for (PotionColour colour : PotionColour.values()) {
            if (object.knowsPotion(colour)) {
                json.writeValue(colour);
            }
        }
        json.writeArrayEnd();
    }

    @Override
    protected PlayerHero createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
        final Demography demography = json.readValue(Demography.class, iter.next());
        final Sex sex = json.readValue(Sex.class, iter.next());
        final int xp = iter.next().asInt();
        final int str = iter.next().asInt();
        final int dex = iter.next().asInt();
        final int wis = iter.next().asInt();
        final int con = iter.next().asInt();
        final ArrayList<PotionColour> knownPotions = new ArrayList<>();
        for (JsonValue color : iter.next().iterator()) {
            knownPotions.add(json.readValue(PotionColour.class, color));
        }

        playerHero = new PlayerHero(demography, sex, level, str, dex, wis, con, inventory);
        for (PotionColour colour : knownPotions) {
            playerHero.addKnownPotion(colour);
        }
        playerHero.getPosition().set(position);

        playerHero.xp = xp;

        return playerHero;
    }

    public PlayerHero lastReadPlayer() {
        if (playerHero == null) {
            throw new IllegalStateException("No player read yet");
        }
        return playerHero;
    }
}
