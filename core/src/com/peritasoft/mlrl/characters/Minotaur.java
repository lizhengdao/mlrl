/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.peritasoft.mlrl.ai.WanderSeekApproach;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.item.Inventory;

public class Minotaur extends Character {
    boolean enemyOnsight;
    int stepsCharging;

    public Minotaur(int level, Position pos) {
        this(level, pos, new Inventory());
        enemyOnsight = false;
        stepsCharging = 0;
    }

    public Minotaur(int level, Position pos, Inventory inventory) {
        super(Demography.MINOTAUR, level, 0, 0, 0, 0, 4, pos.getX(), pos.getY(), new WanderSeekApproach(), inventory);
        //attributes
        this.setStr((int) (level * 1.2f));
        this.setDex(level);
        this.setWis(level / 2);
        this.setCon(level * 3);
        this.resetHp();
    }

    @Override
    public Character findClosestVictim(Level level) {
        Character c = super.findClosestVictim(level);
        if (c == null) {
            enemyOnsight = false;
            stepsCharging = 0;
        } else enemyOnsight = true;
        return c;
    }

    @Override
    public Action move(Direction direction, Level level) {
        if (enemyOnsight) stepsCharging++;
        return super.move(direction, level);
    }

    @Override
    public void attack(Character obj, Level level) {
        this.incStr(Math.min(stepsCharging,7));
        super.attack(obj, level);
        this.decStr(Math.min(stepsCharging,7));
        stepsCharging = 0;
    }
}
