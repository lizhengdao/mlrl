/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

public enum Demography {
    HERO_WARRIOR(Race.HUMAN, Klass.WARRIOR, "Gunther|Sif"),
    HERO_ARCHER(Race.HUMAN, Klass.ARCHER, "Hawke|Diana"),
    HERO_MAGE(Race.HUMAN, Klass.MAGE, "Simon|Lilith"),
    RAT_GIANT(Race.RAT, Klass.GIANT, "Giant Rat"),
    MIMIC(Race.MIMIC, Klass.GRUNT, "Mimic"),
    BEETLE_GIANT(Race.BEETLE, Klass.GIANT, "Giant Beetle"),
    BAT_GIANT(Race.BAT, Klass.GIANT, "Bat"),
    SPIDER_GIANT(Race.SPIDER, Klass.GIANT, "Giant Spider"),
    SLIME(Race.SLIME, Klass.GIANT, "Slime"),
    SLIME_CONJOINED(Race.SLIME, Klass.GIANT, "Small Slimes"),
    SLIME_SMALL(Race.SLIME, Klass.GIANT, "Small Slime"),
    SNAKE(Race.SNAKE, Klass.GIANT, "Venomous Snake"),
    JELLY_CUBE(Race.JELLY, Klass.GIANT, "Jelly Cube"),
    GOBLIN_WARRIOR(Race.GOBLIN, Klass.WARRIOR, "Goblin Warrior"),
    GOBLIN_LANCER(Race.GOBLIN, Klass.LANCER, "Goblin Lancer"),
    GOBLIN_MAGE(Race.GOBLIN, Klass.MAGE, "Goblin Mage"),
    GOBLIN_GRUNT(Race.GOBLIN, Klass.GRUNT, "Goblin Grunt"),
    GOBLIN_ARCHER(Race.GOBLIN, Klass.ARCHER, "Goblin Archer"),
    TROLL(Race.TROLL, Klass.GRUNT, "Troll"),
    SKELETON(Race.SKELETON, Klass.GRUNT, "Skeleton"),
    SKELETON_WARRIOR(Race.SKELETON, Klass.WARRIOR, "Skeleton Warrior"),
    SKELETON_MAGE(Race.SKELETON, Klass.MAGE, "Skeleton Mage"),
    SKELETON_LICH(Race.SKELETON, Klass.LICH, "Skeleton Lich"),
    SKELETON_ARCHER(Race.SKELETON, Klass.ARCHER, "Skeleton Archer"),
    TRASGA_HAMELIN(Race.BOSS, Klass.SUMMONER, "Trasga Hamelin"),
    URMUK(Race.BOSS, Klass.WARRIOR, "Urmuk, the troll whisperer"),
    BEHOLDER(Race.BEHOLDER, Klass.MAGE, "Beholder"),
    EKIMUS(Race.BOSS, Klass.MAGE, "Ekimus"),
    MIST(Race.ELEMENTAL, Klass.MAGE, "Mist"),
    IVREXA(Race.BOSS, Klass.WARRIOR, "Ivrexa, the wise elder"),
    MINOTAUR(Race.MINOTAUR, Klass.GRUNT, "Minotaur"),
    FIRE_ELEMENTAL(Race.ELEMENTAL, Klass.MAGE, "Fire Elemental"),
    WRAITH(Race.WRAITH, Klass.GRUNT, "Wraith"),
    GIANT(Race.GIANT, Klass.GIANT, "Giant"),
    DWARF(Race.DWARF, Klass.WARRIOR, "Dwarf Warrior"),
    DEATH(Race.BOSS, Klass.DEATH, "Death"),
    DARK_KNIGHT(Race.KNIGHT, Klass.WARRIOR, "Dark Knight"),
    FIEND(Race.DEMON, Klass.WARRIOR, "Abyssal Fiend"),
    PHOENIX(Race.ELEMENTAL, Klass.WARRIOR, "Phoenix");

    public final Race race;
    public final Klass klass;
    public final String name;

    Demography(Race race, Klass klass, String name) {
        this.race = race;
        this.klass = klass;
        this.name = name;
    }

    public String getNameBySex(Sex sex) {
        final int separator = name.indexOf('|');
        return sex == Sex.FEMALE ? name.substring(separator + 1) : name.substring(0, separator);
    }
}
