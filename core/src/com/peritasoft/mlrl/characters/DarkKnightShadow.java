/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.ai.WanderSeekApproach;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.Burn;
import com.peritasoft.mlrl.effects.LifeObjType;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.Item;

public class DarkKnightShadow extends Character {

    public DarkKnightShadow(int level, Position pos) {
        this(level, pos, new Inventory());
    }

    public DarkKnightShadow(int level, Position pos, Inventory inventory) {
        super(Demography.DARK_KNIGHT, level, 0, 0, 0, 0, 4, pos.getX(), pos.getY(),
                new WanderSeekApproach(), inventory);
        this.givesXP = false;
        this.setStr((int) (level * 0.80f));
        this.setDex(level);
        this.setWis(level / 2);
        this.setCon(1);
        this.resetHp();
    }

    @Override
    public void attack(Character obj, Level level) {
        if (MathUtils.random(1, 20) + getDex() > MathUtils.random(1, 20) + obj.getDex()) {
            GameEvent.attackHit(this, obj, this.getStr(), ((Item) attackWeapon()).getCategory());
        } else {
            GameEvent.attackMissed(this, obj);
        }
    }

    @Override
    public boolean onKilled(Level level, Character killer) {
        level.getLifeObjs().add(new Burn(getPosition(), LifeObjType.VOID));
        return true;
    }
}
