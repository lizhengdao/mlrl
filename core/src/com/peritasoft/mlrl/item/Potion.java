/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.weapons.Teleport;

public class Potion extends Item {
    private final PotionColour colour;

    public Potion() {
        this(PotionColour.getRandom());
    }

    public Potion(PotionColour colour) {
        super("??? Potion", " ");
        this.colour = colour;
        name = String.valueOf(colour).charAt(0) + String.valueOf(colour).substring(1).toLowerCase() + " Potion";
        description = "This reflective potion is " + String.valueOf(colour).toLowerCase() + " in color. It has no smell but it's bubbling up. What will be its purpose? There's only one way to know it.";
    }

    public Potion(PotionType type) {
        this(PotionVariations.getPotionColour(type));
    }

    public PotionColour getColour() {
        return colour;
    }

    public String getKnownName() {
        return getType().getName();
    }

    public String getKnownDescription() {
        return getType().getDescription();
    }

    @Override
    public ItemCategory getCategory() {
        switch (colour) {
            case RED:
                return ItemCategory.POTION_RED;
            case BLUE:
                return ItemCategory.POTION_BLUE;
            case YELLOW:
                return ItemCategory.POTION_YELLOW;
            case PURPLE:
                return ItemCategory.POTION_PURPLE;
            case BLACK:
                return ItemCategory.POTION_BLACK;
            case GREEN:
                return ItemCategory.POTION_GREEN;
        }
        throw new IllegalStateException("Potion has an invalid colour");
    }

    @Override
    public boolean isEquipable() {
        return false;
    }

    @Override
    public boolean isUsable() {
        return true;
    }

    @Override
    public void use(Character character, Level level) {
        int bonusGained = MathUtils.random(1, 2);
        GameEvent.drank(this, character);
        switch (getType()) {
            case HEALTH:
                character.heal(MathUtils.random(character.getMaxHp() / 2, character.getMaxHp()));
                break;
            case DISPEL:
                character.dispell();
                GameEvent.dispelledEffects(character);
                break;
            case STRENGTH:
                character.setStr(character.getStr() + bonusGained);
                GameEvent.raiseStat(character, "Strength", bonusGained);
                break;
            case DEXTRERY:
                character.setDex(character.getDex() + bonusGained);
                GameEvent.raiseStat(character, "Dextrery", bonusGained);
                break;
            case WISDOM:
                character.setWis(character.getWis() + bonusGained);
                GameEvent.raiseStat(character, "Wisdom", bonusGained);
                break;
            case TELEPORT:
                Teleport teleport = new Teleport();
                teleport.cast(character, level);
                break;
        }
        if (character instanceof PlayerHero) ((PlayerHero) character).addKnownPotion(colour);

    }

    public PotionType getType() {
        return PotionVariations.getPotionType(colour);
    }

}
