/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.weapons.Teleport;

public class ScrollOfTeleport extends Item implements Scroll {
    private final int level;

    public ScrollOfTeleport(int level) {
        super("Scroll of Teleport", "This spell scroll bears the words of a single teleport spell, written in a mystical cipher.");
        this.level = level + 1;
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.SCROLL;
    }

    @Override
    public boolean isEquipable() {
        return false;
    }

    @Override
    public boolean isUsable() {
        return true;
    }

    @Override
    public void use(Character character, Level level) {
        Teleport teleport = new Teleport();
        teleport.cast(character, level);
        GameEvent.teleported();
    }

    public int getLevel() {
        return level - 1;
    }
}
