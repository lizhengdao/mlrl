/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;

public abstract class KindOfWeapon extends Item {

    private final int bonusStr;
    private final int bonusDex;
    private final int bonusCon;
    private final int bonusWis;
    private final int bonusDamage;
    protected Character owner;

    public KindOfWeapon(String name, String description, int bonusStr, int bonusDex, int bonusCon, int bonusWis, int bonusDamage) {
        super(name, description);
        this.bonusStr = bonusStr;
        this.bonusDex = bonusDex;
        this.bonusCon = bonusCon;
        this.bonusWis = bonusWis;
        this.bonusDamage = bonusDamage;
    }

    public abstract KindOfWeapon copy();

    @Override
    public boolean isEquipable() {
        return true;
    }

    @Override
    public boolean isUsable() {
        return false;
    }

    @Override
    public void use(Character character, Level level) {
        throw new IllegalStateException("Weapons can not be used directly");
    }

    public int getBonusStr() {
        return bonusStr;
    }

    public int getBonusDex() {
        return bonusDex;
    }

    public int getBonusCon() {
        return bonusCon;
    }

    public int getBonusWis() {
        return bonusWis;
    }

    public int getBonusDamage() {
        return bonusDamage;
    }

    public int rollBonusDamage() {
        return MathUtils.random(bonusDamage);
    }

    public void onEquiped(Character character) {
        character.incStr(getBonusStr());
        character.incDex(getBonusDex());
        character.incWis(getBonusWis());
        character.incCon(getBonusCon());
        owner = character;
    }

    public void onUnequiped(Character character) {
        character.decStr(getBonusStr());
        character.decDex(getBonusDex());
        character.decWis(getBonusWis());
        character.decCon(getBonusCon());
        owner = null;
    }

    public abstract int getQualityIndex(Character wielder);
}
