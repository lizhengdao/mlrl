/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.Item;

import java.util.ArrayList;

public class InventorySerializer implements Json.Serializer<Inventory> {

    @Override
    public void write(Json json, Inventory object, Class knownType) {
        json.writeArrayStart();
        json.writeValue(object.countArrows());
        json.writeArrayStart();
        for (final Item item : object) {
            MyLittleSerializer.writeObjectValue(json, item);
        }
        json.writeArrayEnd();
        json.writeArrayEnd();
    }

    @Override
    public Inventory read(Json json, JsonValue jsonData, Class type) {
        final JsonValue.JsonIterator iter = jsonData.iterator();
        int arrows = iter.next().asInt();
        final ArrayList<Item> items = new ArrayList<>();
        for (JsonValue itemObject : iter.next()) {
            final Item item = MyLittleSerializer.readObjectValue(json, itemObject);
            items.add(item);
        }
        return new Inventory(arrows, items);
    }
}
