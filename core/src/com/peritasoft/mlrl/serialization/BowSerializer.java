/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.weapons.Bow;

class BowSerializer implements Json.Serializer<Bow> {
    @Override
    public void write(Json json, Bow object, Class knownType) {
        json.writeArrayStart();
        json.writeValue(object.getRange());
        json.writeValue(object.getBonusStr());
        json.writeValue(object.getBonusDex());
        json.writeValue(object.getBonusCon());
        json.writeValue(object.getBonusWis());
        json.writeValue(object.getBonusDamage());

        json.writeArrayEnd();
    }

    @Override
    public Bow read(Json json, JsonValue jsonData, Class type) {
        final int[] stats = jsonData.asIntArray();
        if (stats.length != 6) {
            throw new IllegalArgumentException("Incorrect stats for bow");
        }
        final int range = stats[0];
        final int bonusStr = stats[1];
        final int bonusDex = stats[2];
        final int bonusCon = stats[3];
        final int bonusWis = stats[4];
        final int bonusDamage = stats[5];
        return new Bow(range, bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
    }
}
