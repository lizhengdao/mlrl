/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.SerializationException;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.*;
import com.peritasoft.mlrl.dungeongen.*;
import com.peritasoft.mlrl.events.Log;
import com.peritasoft.mlrl.item.*;
import com.peritasoft.mlrl.props.*;
import com.peritasoft.mlrl.scores.Score;
import com.peritasoft.mlrl.screens.PlayScreen;
import com.peritasoft.mlrl.weapons.*;

import java.util.ArrayList;

public class MyLittleSerializer {

    private static final String NAMESPACE = "com.peritasoft.mlrl";
    private static final int NAMESPACE_LENGTH = NAMESPACE.length();

    public static void registerClasses(MyLittleRogueLike game, Json json) {
        final PlayerHeroSerializer playerHeroSerializer = new PlayerHeroSerializer();
        json.setSerializer(Arrow.class, new ArrowSerializer());
        json.setSerializer(PlayScreen.class, new PlayScreenSerializer(game));
        json.setSerializer(PlayerHero.class, playerHeroSerializer);
        json.setSerializer(Color.class, new ColorSerializer());
        json.setSerializer(Position.class, new PositionSerializer());
        json.setSerializer(Log.class, new LogSerializer(playerHeroSerializer));
        json.setSerializer(Dungeon.class, new DungeonSerializer(playerHeroSerializer));
        json.setSerializer(MemoizedLevelGenerator.class, new MemoizedLevelGeneratorSerializer());
        json.setSerializer(PotionVariations.class, new PotionVariationsSerializer());
        json.setSerializer(Bookshelf.class, new BookshelfSerializer());
        json.setSerializer(Heart.class, new HeartSerializer());
        json.setSerializer(Altar.class, new AltarSerializer());
        json.setSerializer(Statue.class, new StatueSerializer());
        json.setSerializer(Coins.class, new CoinsSerializer());
        json.setSerializer(Score.class, new ScoreSerializer());

        json.setSerializer(Level.class, new LevelSerializer<Level>() {
            @Override
            protected void writeOwnProperties(Json json, Level object) {
                // Nothing to do for base Level.
            }

            @Override
            protected Level createInstance(JsonValue.JsonIterator iter, Tile[][] tiles, Position startingPosition, LevelType levelType) {
                return new Level(tiles, startingPosition, levelType);
            }
        });

        json.setSerializer(BossLevel.class, new LevelSerializer<BossLevel>() {
            @Override
            protected void writeOwnProperties(Json json, BossLevel object) {
                json.writeArrayStart();
                for (String message : object.getLogMessages()) {
                    json.writeValue(message);
                }
                json.writeArrayEnd();
            }

            @Override
            protected BossLevel createInstance(JsonValue.JsonIterator iter, Tile[][] tiles, Position startingPosition, LevelType levelType) {
                final ArrayList<String> messages = new ArrayList<>();
                for (JsonValue messageData : iter.next().iterator()) {
                    messages.add(messageData.asString());
                }
                return new BossLevel(tiles, startingPosition, levelType, messages.toArray(new String[0]));
            }
        });

        json.setSerializer(Bow.class, new BowSerializer());
        json.setSerializer(FireSpellgem.class, new FireSpellgemSerializer());
        json.setSerializer(Inventory.class, new InventorySerializer());
        json.setSerializer(Potion.class, new PotionSerializer());

        json.setSerializer(FireGrimoire.class, new GrimoireSerializer<FireGrimoire>() {
            @Override
            protected FireGrimoire createGrimoire(int minWis, int manaCost, int minDistance, int bonusDamage) {
                return new FireGrimoire(minWis, manaCost, minDistance, bonusDamage);
            }
        });

        json.setSerializer(IceGrimoire.class, new GrimoireSerializer<IceGrimoire>() {
            @Override
            protected IceGrimoire createGrimoire(int minWis, int manaCost, int minDistance, int bonusDamage) {
                return new IceGrimoire(minWis, manaCost, minDistance, bonusDamage);
            }
        });

        json.setSerializer(LightningGrimoire.class, new GrimoireSerializer<LightningGrimoire>() {
            @Override
            protected LightningGrimoire createGrimoire(int minWis, int manaCost, int minDistance, int bonusDamage) {
                return new LightningGrimoire(minWis, manaCost, minDistance, bonusDamage);
            }
        });

        json.setSerializer(PoisonGrimoire.class, new GrimoireSerializer<PoisonGrimoire>() {
            @Override
            protected PoisonGrimoire createGrimoire(int minWis, int manaCost, int minDistance, int bonusDamage) {
                return new PoisonGrimoire(minWis, manaCost, minDistance, bonusDamage);
            }
        });

        json.setSerializer(Dagger.class, new MeleeWeaponSerializer<Dagger>() {
            @Override
            protected Dagger createWeapon(int bonusStr, int bonusDex, int bonusCon, int bonusWis, int bonusDamage) {
                return new Dagger(bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
            }
        });

        json.setSerializer(Lance.class, new MeleeWeaponSerializer<Lance>() {
            @Override
            protected Lance createWeapon(int bonusStr, int bonusDex, int bonusCon, int bonusWis, int bonusDamage) {
                return new Lance(bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
            }
        });

        json.setSerializer(Sword.class, new MeleeWeaponSerializer<Sword>() {
            @Override
            protected Sword createWeapon(int bonusStr, int bonusDex, int bonusCon, int bonusWis, int bonusDamage) {
                return new Sword(bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
            }
        });

        json.setSerializer(ScrollOfFire.class, new ScrollSerializer<ScrollOfFire>() {
            @Override
            protected ScrollOfFire createScroll(int level) {
                return new ScrollOfFire(level);
            }
        });

        json.setSerializer(ScrollOfIce.class, new ScrollSerializer<ScrollOfIce>() {
            @Override
            protected ScrollOfIce createScroll(int level) {
                return new ScrollOfIce(level);
            }
        });

        json.setSerializer(ScrollOfPetrification.class, new ScrollSerializer<ScrollOfPetrification>() {
            @Override
            protected ScrollOfPetrification createScroll(int level) {
                return new ScrollOfPetrification(level);
            }
        });

        json.setSerializer(ScrollOfPoison.class, new ScrollSerializer<ScrollOfPoison>() {
            @Override
            protected ScrollOfPoison createScroll(int level) {
                return new ScrollOfPoison(level);
            }
        });

        json.setSerializer(ScrollOfTeleport.class, new ScrollSerializer<ScrollOfTeleport>() {
            @Override
            protected ScrollOfTeleport createScroll(int level) {
                return new ScrollOfTeleport(level);
            }
        });

        json.setSerializer(Bat.class, new CharacterSerializer<Bat>() {
            @Override
            protected Bat createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Bat(level, position, inventory);
            }
        });

        json.setSerializer(Beetle.class, new CharacterSerializer<Beetle>() {
            @Override
            protected Beetle createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Beetle(level, position, inventory);
            }
        });

        json.setSerializer(Beholder.class, new CharacterSerializer<Beholder>() {
            @Override
            protected Beholder createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Beholder(level, position, inventory);
            }
        });

        json.setSerializer(Dwarf.class, new CharacterSerializer<Dwarf>() {
            @Override
            protected Dwarf createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Dwarf(level, position, inventory);
            }
        });

        json.setSerializer(Ekimus.class, new CharacterSerializer<Ekimus>() {
            @Override
            protected Ekimus createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Ekimus(level, position, inventory);
            }
        });

        json.setSerializer(FireElemental.class, new CharacterSerializer<FireElemental>() {
            @Override
            protected FireElemental createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new FireElemental(level, position, inventory);
            }
        });

        json.setSerializer(Giant.class, new CharacterSerializer<Giant>() {
            @Override
            protected Giant createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Giant(level, position, inventory);
            }
        });

        json.setSerializer(GoblinArcher.class, new CharacterSerializer<GoblinArcher>() {
            @Override
            protected GoblinArcher createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new GoblinArcher(level, position, inventory);
            }
        });

        json.setSerializer(GoblinGrunt.class, new CharacterSerializer<GoblinGrunt>() {
            @Override
            protected GoblinGrunt createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new GoblinGrunt(level, position, inventory);
            }
        });

        json.setSerializer(GoblinLancer.class, new CharacterSerializer<GoblinLancer>() {
            @Override
            protected GoblinLancer createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new GoblinLancer(level, position, inventory);
            }
        });

        json.setSerializer(GoblinMage.class, new CharacterSerializer<GoblinMage>() {
            @Override
            protected GoblinMage createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new GoblinMage(level, position, inventory);
            }
        });

        json.setSerializer(GoblinWarrior.class, new CharacterSerializer<GoblinWarrior>() {
            @Override
            protected GoblinWarrior createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new GoblinWarrior(level, position, inventory);
            }
        });

        json.setSerializer(Ivrexa.class, new CharacterSerializer<Ivrexa>() {
            @Override
            protected Ivrexa createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Ivrexa(level, position, inventory);
            }
        });

        json.setSerializer(JellyCube.class, new CharacterSerializer<JellyCube>() {
            @Override
            protected JellyCube createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new JellyCube(level, position, inventory);
            }
        });

        json.setSerializer(Mimic.class, new CharacterSerializer<Mimic>() {
            @Override
            protected Mimic createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Mimic(level, position, inventory);
            }
        });

        json.setSerializer(Minotaur.class, new MinotaurSerializer());

        json.setSerializer(Rat.class, new CharacterSerializer<Rat>() {
            @Override
            protected void writeOwnProperties(Json json, Rat object) {
                json.writeValue(object.doesGiveXP());
            }

            @Override
            protected Rat createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                final boolean givesXp = iter.next().asBoolean();
                return new Rat(level, position, givesXp, inventory);
            }
        });

        json.setSerializer(Skeleton.class, new SkeletonSerializer<Skeleton>() {
            @Override
            protected Skeleton createInstance(int level, Position position, boolean givesXp, Inventory inventory) {
                return new Skeleton(level, position, givesXp, inventory);
            }
        });

        json.setSerializer(SkeletonArcher.class, new SkeletonSerializer<SkeletonArcher>() {
            @Override
            protected SkeletonArcher createInstance(int level, Position position, boolean givesXp, Inventory inventory) {
                return new SkeletonArcher(level, position, inventory);
            }
        });

        json.setSerializer(SkeletonLich.class, new SkeletonSerializer<SkeletonLich>() {
            @Override
            protected SkeletonLich createInstance(int level, Position position, boolean givesXp, Inventory inventory) {
                return new SkeletonLich(level, position, inventory);
            }
        });

        json.setSerializer(SkeletonMage.class, new SkeletonSerializer<SkeletonMage>() {
            @Override
            protected SkeletonMage createInstance(int level, Position position, boolean givesXp, Inventory inventory) {
                return new SkeletonMage(level, position, inventory);
            }
        });

        json.setSerializer(SkeletonWarrior.class, new SkeletonSerializer<SkeletonWarrior>() {
            @Override
            protected SkeletonWarrior createInstance(int level, Position position, boolean givesXp, Inventory inventory) {
                return new SkeletonWarrior(level, position, inventory);
            }
        });

        json.setSerializer(Slime.class, new CharacterSerializer<Slime>() {
            @Override
            protected Slime createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Slime(level, position, inventory);
            }
        });

        json.setSerializer(SlimeConjoined.class, new CharacterSerializer<SlimeConjoined>() {
            @Override
            protected SlimeConjoined createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new SlimeConjoined(level, position, inventory);
            }
        });

        json.setSerializer(SlimeSmall.class, new CharacterSerializer<SlimeSmall>() {
            @Override
            protected SlimeSmall createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new SlimeSmall(level, position, inventory);
            }
        });

        json.setSerializer(Snake.class, new CharacterSerializer<Snake>() {
            @Override
            protected Snake createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Snake(level, position, inventory);
            }
        });

        json.setSerializer(Spider.class, new CharacterSerializer<Spider>() {
            @Override
            protected Spider createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Spider(level, position, inventory);
            }
        });

        json.setSerializer(TrasgaHamelin.class, new CharacterSerializer<TrasgaHamelin>() {
            @Override
            protected TrasgaHamelin createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new TrasgaHamelin(level, position, inventory);
            }
        });

        json.setSerializer(Troll.class, new CharacterSerializer<Troll>() {
            @Override
            protected Troll createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Troll(level, position, inventory);
            }
        });

        json.setSerializer(Urmuk.class, new CharacterSerializer<Urmuk>() {
            @Override
            protected Urmuk createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Urmuk(level, position, inventory);
            }
        });

        json.setSerializer(Wraith.class, new CharacterSerializer<Wraith>() {
            @Override
            protected Wraith createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
                return new Wraith(level, position, inventory);
            }
        });

        json.setSerializer(Chest.class, new ChestSerializer<Chest>() {
            @Override
            protected void writeOwnProperties(Json json, Chest object) {
                MyLittleSerializer.writeObjectValue(json, object.getItem());
            }

            @Override
            protected Chest createInstance(Json json, JsonValue.JsonIterator iter) {
                final Item item = MyLittleSerializer.readObjectValue(json, iter.next());
                return new Chest(item);
            }
        });

        json.setSerializer(ChestEmpty.class, new ChestSerializer<ChestEmpty>() {
            @Override
            protected void writeOwnProperties(Json json, ChestEmpty object) {
                // Nothing to do for empty chest
            }

            @Override
            protected ChestEmpty createInstance(Json json, JsonValue.JsonIterator iter) {
                return new ChestEmpty();
            }
        });

        json.setSerializer(MimicDecoy.class, new ChestSerializer<MimicDecoy>() {
            @Override
            protected void writeOwnProperties(Json json, MimicDecoy object) {
                MyLittleSerializer.writeObjectValue(json, object.getEnemy());
            }

            @Override
            protected MimicDecoy createInstance(Json json, JsonValue.JsonIterator iter) {
                Character enemy = MyLittleSerializer.readObjectValue(json, iter.next());
                return new MimicDecoy(enemy);
            }
        });
    }

    private static String getClassName(Object object) {
        final String className = object.getClass().getName();
        return className.substring(NAMESPACE_LENGTH);
    }

    public static void writeObjectValue(Json json, Object object) {
        json.writeObjectStart();
        if (object == null) {
            json.writeValue("", (Object) null);
        } else {
            json.writeValue(getClassName(object), object);
        }
        json.writeObjectEnd();
    }

    @SuppressWarnings("unchecked")
    public static <T> T readObjectValue(Json json, JsonValue parent) {
        final JsonValue object = parent.child;
        final String className = NAMESPACE + object.name;
        if (className.equals(NAMESPACE)) {
            return null;
        }
        return (T) json.readValue(getClass(json, className), object);
    }

    @SuppressWarnings("unchecked")
    private static <T> Class<T> getClass(Json json, String className) {
        Class<T> type = json.getClass(className);
        if (type == null) {
            try {
                type = (Class<T>) ClassReflection.forName(className);
            } catch (ReflectionException ex) {
                throw new SerializationException(ex);
            }
        }
        return type;
    }

    private MyLittleSerializer() {
        // Nothing to do; keep class “static”
    }
}
