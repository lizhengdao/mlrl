/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.item.Arrow;

public class ArrowSerializer implements Json.Serializer<Arrow> {
    @Override
    public void write(Json json, Arrow object, Class knownType) {
        json.writeValue(object.countArrows());
    }

    @Override
    public Arrow read(Json json, JsonValue jsonData, Class type) {
        final int arrowCount = jsonData.asInt();

        return new Arrow(arrowCount);
    }
}
