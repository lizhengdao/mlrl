/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.ai.PathFinder;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class LevelUtils {
    static int[] countSurroundingWallsFloors(Tile[][] tiles, int x, int y, int radius) {
        int[] wallsFloors = new int[2];
        for (int oy = -radius; oy <= radius; oy++) {
            for (int ox = -radius; ox <= radius; ox++) {
                if (x + ox < 0 || x + ox >= tiles[0].length || y + oy < 0 || y + oy >= tiles.length) {
                    continue;
                }
                if (tiles[y + oy][x + ox] == Tile.GROUND || tiles[y + oy][x + ox] == Tile.LAVA) {
                    wallsFloors[1]++;
                } else {
                    wallsFloors[0]++;
                }
            }
        }
        return wallsFloors;
    }

    static int countSurroundingGroundTiles(Level level, Position pos, int radius) {
        int count = 0;
        for (int y = -radius; y <= radius; y++) {
            final int row = pos.getY() + y;
            for (int x = -radius; x <= radius; x++) {
                final int col = pos.getX() + x;
                if (level.isGround(col, row)) {
                    count++;
                }
            }
        }
        return count;
    }

    static Position putStaircases(Tile[][] tiles, boolean upStaircase, boolean downStaircase) {
        Position up = findValidPosition(tiles);
        if (upStaircase) {
            tiles[up.getY()][up.getX()] = Tile.STAIRCASEUP;
        }
        if (downStaircase) {
            int minSteps = (int) Math.sqrt(tiles.length * tiles.length + tiles[0].length * tiles[0].length) / 2;

            final Tilemap level = new Tilemap(tiles);
            Position down = findValidPosition(tiles);
            int tries = 1000;
            while (!hasPathOfAtLeast(level, up, down, minSteps)) {
                tries--;
                if (tries == 0) {
                    tries = 1000;
                    minSteps /= 2;
                }
                down = findValidPosition(tiles);
            }
            tiles[down.getY()][down.getX()] = Tile.STAIRCASEDOWN;
        }
        return up;
    }

    private static boolean hasPathOfAtLeast(Tilemap level, Position start, Position end, int minSteps) {
        Queue<Position> path = PathFinder.find(level, start, end);
        return path != null && path.size() >= minSteps;
    }

    public static class Tilemap implements PathFinder.TraverseableMap {
        private final Tile[][] tiles;
        private final int width;
        private final int height;

        Tilemap(Tile[][] tiles) {
            this.tiles = tiles;
            height = tiles.length;
            width = tiles[0].length;
        }

        @Override
        public boolean isTraverseable(int x, int y) {
            return x >= 0 && x < getWidth() && y >= 0 && y <= getHeight() && tiles[y][x].isWalkable;
        }

        @Override
        public int getHeight() {
            return height;
        }

        @Override
        public int getWidth() {
            return width;
        }
    }

    public static Position findValidPosition(Tile[][] tiles) {
        final int height = tiles.length;
        final int width = tiles[0].length;
        Position pos = new Position(MathUtils.random(width - 1), MathUtils.random(height - 1));

        if (tiles[pos.getY()][pos.getX()] == Tile.GROUND
                && LevelUtils.countSurroundingWallsFloors(tiles, pos.getX(), pos.getY(), 1)[1] > 8) {
            return pos;
        } else {
            return findValidPosition(tiles);
        }
    }

    public static Position findValidPosition(final Level level) {
        final int height = level.getHeight();
        final int width = level.getWidth();
        while (true) {
            Position pos = new Position(MathUtils.random(width - 1), MathUtils.random(height - 1));
            if (level.isGround(pos) && countSurroundingGroundTiles(level, pos, 1) > 8) {
                final Queue<Position> path = PathFinder.find(level, pos, level.getStartingPosition(), new PathFinder.Traversor() {
                    @Override
                    public boolean isTraverseable(int x, int y) {
                        return level.isWalkable(x, y);
                    }
                });
                if (path != null) {
                    return pos;
                }
            }
        }
    }

    static int reachableArea(Level level, Position start) {
        final int height = level.getHeight();
        if (level.getHeight() <= start.getY()) return 0;
        final int width = level.getWidth();
        if (width <= start.getX()) return 0;

        if (!level.isWalkable(start)) {
            return 0;
        }

        int count = 0;
        boolean[][] visited = new boolean[height][width];
        visited[start.getY()][start.getX()] = true;
        Deque<Position> queue = new LinkedList<>();
        queue.push(start);
        while (!queue.isEmpty()) {
            count++;
            Position pos = queue.pop();
            for (Direction dir : Direction.values()) {
                final int x = pos.getX() + dir.x;
                final int y = pos.getY() + dir.y;
                if (x >= 0 && x < width && y >= 0 && y < height && !visited[y][x] && level.isWalkable(x, y)) {
                    visited[y][x] = true;
                    queue.push(new Position(x, y));
                }
            }
        }
        return count;
    }

    static public boolean findStaircaseInRoom(Tile[][] tiles, Position start) {
        final int height = tiles.length;
        if (height <= start.getY()) return false;
        final int width = tiles[start.getY()].length;
        if (width <= start.getX()) return false;

        boolean[][] visited = new boolean[height][width];
        Deque<Position> queue = new LinkedList<>();
        queue.push(start);
        while (!queue.isEmpty()) {
            Position pos = queue.pop();
            if (visited[pos.getY()][pos.getX()]) {
                continue;
            }
            if (tiles[pos.getY()][pos.getX()] == Tile.STAIRCASEDOWN) {
                return true;
            }
            visited[pos.getY()][pos.getX()] = true;
            for (Direction dir : Direction.values()) {
                final int x = pos.getX() + dir.x;
                final int y = pos.getY() + dir.y;
                if (x >= 0 && x < width && y >= 0 && y < height && tiles[y][x].isWalkable && !visited[y][x]) {
                    queue.push(new Position(x, y));
                }
            }
        }
        return false;
    }

}
