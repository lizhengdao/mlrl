/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.item.Arrow;

public class ArrowPutter implements LevelGenerator {
    private final LevelGenerator next;

    public ArrowPutter(LevelGenerator next) {
        if (next == null) {
            throw new IllegalArgumentException("next generator can not be null");
        }
        this.next = next;
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        final Level level = next.generate(floor, upStaircase, player);
        if (player.getInventory().hasABow()) {
            addArrows(level, player);
        }
        return level;
    }

    private void addArrows(Level level, PlayerHero player) {
        int numArrows = Math.max(5, MathUtils.random(25, 35) - player.getInventory().countArrows());
        while (numArrows > 0) {
            final int arrowBundle = Math.min(MathUtils.random(4, 10), numArrows);
            Position pos = LevelUtils.findValidPosition(level);
            Cell cell = level.getCell(pos);
            if (!cell.isEmpty()) {
                continue;
            }
            cell.setItem(new Arrow(arrowBundle));
            numArrows -= arrowBundle;
        }
    }

    public LevelGenerator getNext() {
        return next;
    }

}
