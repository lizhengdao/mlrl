/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.characters.Rat;
import com.peritasoft.mlrl.characters.TrasgaHamelin;
import com.peritasoft.mlrl.effects.LifeObjLayer;
import com.peritasoft.mlrl.effects.Web;
import com.peritasoft.mlrl.props.Bookshelf;
import com.peritasoft.mlrl.props.Claudron;
import com.peritasoft.mlrl.props.Torch;

import java.util.Arrays;

public class TrasgaRoomLevelGenerator implements LevelGenerator {
    private static final int ROOM_WIDTH = 15;
    private static final int ROOM_HEIGHT = 15;

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        //Room width/Heith is constant
        Tile[][] tiles = new Tile[ROOM_HEIGHT][ROOM_WIDTH];
        fillWithGround(tiles);
        putWallsAndColumns(tiles);
        Position startingPosition = new Position(2, 7);
        String[] logMessages = new String[]{
                "You can hear some rats squeaking and a strange voice who commands them",
                "Bend before the mighty Trasga of Hamelin"
        };
        final Level level = new BossLevel(tiles, startingPosition, LevelType.SEWER, logMessages);
        generateBoss(level, floor);
        decorateRoom(level);
        return level;
    }

    private void fillWithGround(Tile[][] tiles) {
        for (Tile[] row : tiles) {
            Arrays.fill(row, Tile.GROUND);
        }
    }

    private void putWallsAndColumns(Tile[][] tiles) {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[0].length; j++) {
                if (i == 0 || i == tiles.length - 1 || j == 0 || j == tiles[0].length - 1) {
                    tiles[i][j] = Tile.WALL;
                }
            }
        }

        //Colums
        tiles[ROOM_HEIGHT / 3 - 1][ROOM_WIDTH / 3 - 1] = Tile.WALL;
        tiles[ROOM_HEIGHT / 3][ROOM_WIDTH / 3 - 1] = Tile.WALL;
        tiles[ROOM_HEIGHT / 3 - 1][ROOM_WIDTH / 3] = Tile.WALL;
        tiles[ROOM_HEIGHT / 3][ROOM_WIDTH / 3] = Tile.WALL;

        tiles[ROOM_HEIGHT / 3 - 1][(ROOM_WIDTH / 3) * 2 - 1] = Tile.WALL;
        tiles[ROOM_HEIGHT / 3][(ROOM_WIDTH / 3) * 2 - 1] = Tile.WALL;
        tiles[ROOM_HEIGHT / 3 - 1][(ROOM_WIDTH / 3) * 2] = Tile.WALL;
        tiles[ROOM_HEIGHT / 3][(ROOM_WIDTH / 3) * 2] = Tile.WALL;

        tiles[(ROOM_HEIGHT / 3) * 2 - 1][ROOM_WIDTH / 3 - 1] = Tile.WALL;
        tiles[(ROOM_HEIGHT / 3) * 2][ROOM_WIDTH / 3 - 1] = Tile.WALL;
        tiles[(ROOM_HEIGHT / 3) * 2 - 1][ROOM_WIDTH / 3] = Tile.WALL;
        tiles[(ROOM_HEIGHT / 3) * 2][ROOM_WIDTH / 3] = Tile.WALL;

        tiles[(ROOM_HEIGHT / 3) * 2 - 1][(ROOM_WIDTH / 3) * 2 - 1] = Tile.WALL;
        tiles[(ROOM_HEIGHT / 3) * 2][(ROOM_WIDTH / 3) * 2 - 1] = Tile.WALL;
        tiles[(ROOM_HEIGHT / 3) * 2 - 1][(ROOM_WIDTH / 3) * 2] = Tile.WALL;
        tiles[(ROOM_HEIGHT / 3) * 2][(ROOM_WIDTH / 3) * 2] = Tile.WALL;

    }

    private void generateBoss(Level level, int floor) {
        level.addEnemy(new TrasgaHamelin(floor + 3, new Position(8,7)));
        level.addEnemy(new Rat(floor - 1, new Position(4,7), true));
        level.addEnemy(new Rat(floor - 1, new Position(10,7), true));
        level.addEnemy(new Rat(floor - 1, new Position(7,4), true));
        level.addEnemy(new Rat(floor - 1, new Position(7,10), true));
    }

    private void decorateRoom(Level level) {
        level.getCell(7,7).setProp(new Claudron());
        level.getCell(3, 14).setProp(new Torch());
        level.getCell(7, 14).setProp(new Torch());
        level.getCell(11, 14).setProp(new Torch());
        level.getCell(5, 13).setProp(new Bookshelf(12));
        level.getCell(9, 13).setProp(new Bookshelf(12));
        level.getCell(ROOM_HEIGHT / 3, ROOM_WIDTH / 3 - 1).setProp(new Torch());
        level.getCell(ROOM_HEIGHT / 3, (ROOM_WIDTH / 3) * 2 - 1).setProp(new Torch());
        level.getCell((ROOM_HEIGHT / 3) * 2, ROOM_WIDTH / 3 - 1).setProp(new Torch());
        level.getCell((ROOM_HEIGHT / 3) * 2, (ROOM_WIDTH / 3) * 2 - 1).setProp(new Torch());
        level.getCell(ROOM_HEIGHT / 3 - 1, ROOM_WIDTH / 3 - 1).setProp(new Torch());
        level.getCell(ROOM_HEIGHT / 3 - 1, (ROOM_WIDTH / 3) * 2 - 1).setProp(new Torch());
        level.getCell((ROOM_HEIGHT / 3) * 2 - 1, ROOM_WIDTH / 3 - 1).setProp(new Torch());
        level.getCell((ROOM_HEIGHT / 3) * 2 - 1, (ROOM_WIDTH / 3) * 2 - 1).setProp(new Torch());
        level.add(new Web(new Position(1,13), 0, LifeObjLayer.OVER));
        level.add(new Web(new Position(13,13), 1, LifeObjLayer.OVER));
        level.add(new Web(new Position(1,1), 2, LifeObjLayer.OVER));
        level.add(new Web(new Position(13,1), 3, LifeObjLayer.OVER));
    }

}
