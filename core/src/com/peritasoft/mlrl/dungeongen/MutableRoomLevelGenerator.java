/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.peritasoft.mlrl.characters.Death;
import com.peritasoft.mlrl.characters.PlayerHero;

import java.util.Arrays;

public class MutableRoomLevelGenerator implements LevelGenerator {

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        Death death = new Death(50, new Position(5, 8), player);
        Position startingPosition = new Position(5, 1);
        final int height = 10;
        final int width = 10;
        Tile[][] tiles = new Tile[height][width];
        fillWithGround(tiles);
        putWalls(tiles);
        String[] logMessages = new String[]{
                "I was waiting so long for you",
                "This is the end of your journey"
        };
        MutableLevel level = new MutableLevel(tiles, startingPosition, death, logMessages);
        decorateRoom(level);
        return level;
    }

    private void decorateRoom(MutableLevel level) {
        for (int y = 1; y < level.getHeight() - 1; y++) {
            int variation = y % 2;
            for (int x = 1; x < level.getWidth() - 1; x++) {
                level.getCell(x, y).variation = variation++ % 2;
            }
        }
    }

    private void fillWithGround(Tile[][] tiles) {
        for (Tile[] row : tiles) {
            Arrays.fill(row, Tile.GROUND);
        }
    }

    private void putWalls(Tile[][] tiles) {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[0].length; j++) {
                if (i == 0 || i == tiles.length - 1 || j == 0 || j == tiles[0].length - 1) {
                    tiles[i][j] = Tile.WALL;
                }
            }
        }
    }
}
