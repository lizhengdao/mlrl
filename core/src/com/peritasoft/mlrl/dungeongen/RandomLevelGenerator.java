/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.PlayerHero;

public class RandomLevelGenerator implements LevelGenerator {

    private final LevelGenerator[] generators;

    public RandomLevelGenerator(LevelGenerator[] generators) {
        if (generators == null || generators.length == 0) {
            throw new IllegalArgumentException("Generators array can not be NULL nor empty");
        }
        this.generators = generators;
    }

    public LevelGenerator[] getGenerators() {
        return generators;
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        LevelGenerator generator = generators[MathUtils.random(generators.length - 1)];
        return generator.generate(floor, upStaircase, player);
    }

}
