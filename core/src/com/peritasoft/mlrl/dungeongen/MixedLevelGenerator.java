/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.peritasoft.mlrl.characters.PlayerHero;

public class MixedLevelGenerator implements LevelGenerator {

    private final GeneratorFloor[] generators;

    public MixedLevelGenerator(final GeneratorFloor[] generators) {
        if (generators == null) {
            throw new IllegalArgumentException("generators");
        }
        if (generators.length == 0) {
            throw new IllegalArgumentException("generators can not be empty");
        }
        this.generators = generators;
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        for (GeneratorFloor g : generators) {
            if (floor < g.floor) {
                return g.generator.generate(floor, upStaircase, player);
            }
        }
        return generators[generators.length - 1].generator.generate(floor, upStaircase, player);
    }

    public GeneratorFloor[] getGenerators() {
        return generators;
    }

    public static class GeneratorFloor {
        final public LevelGenerator generator;
        final public int floor;

        public GeneratorFloor(final int floor, final LevelGenerator generator) {
            this.generator = generator;
            this.floor = floor;
        }
    }
}
