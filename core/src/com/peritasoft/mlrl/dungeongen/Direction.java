/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;
import java.util.Collections;

public enum Direction {
    NORTH(0, 1),
    WEST(-1, 0),
    SOUTH(0, -1),
    EAST(1, 0),
    NONE(0, 0);

    public final int x;
    public final int y;

    Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Direction atRandom() {
        return Direction.values()[MathUtils.random(Direction.values().length - 1)];
    }

    public static Direction[] randomizedDirections() {
        ArrayList<Direction> randoms = new ArrayList<>();
        for (Direction dir: Direction.values()) {
            if (dir != NONE) randoms.add(dir);
        }
        Collections.shuffle(randoms);
        return randoms.toArray(new Direction[4]);
    }
}
