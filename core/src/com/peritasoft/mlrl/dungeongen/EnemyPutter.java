/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;

public abstract class EnemyPutter implements LevelGenerator {
    private final LevelGenerator next;
    private final int lowerBound;

    public EnemyPutter(int lowerBound, LevelGenerator next) {
        if (next == null) {
            throw new IllegalArgumentException("next level generator can not be null");
        }
        this.next = next;
        this.lowerBound = lowerBound;
    }

    public LevelGenerator getNext() {
        return next;
    }

    public int getLowerBound() {
        return lowerBound;
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        for (; ; ) {
            Level level = next.generate(floor, upStaircase, player);
            int reachableArea = LevelUtils.reachableArea(level, level.getStartingPosition());
            int numEnemies = reachableArea / 23;
            if (numEnemies < lowerBound) {
                continue; // create a new random level and try again
            }
            generateEnemies(numEnemies, level, floor, player.getLevel());
            return level;
        }
    }

    protected void generateEnemies(int numEnemies, Level level, int floor, int playerLevel) {
        for (int i = 0; i < numEnemies; i++) {
            Position pos;
            do {
                pos = findValidPosition(level);
            } while (level.hasCharacterIn(pos));
            Character c = generateEnemy(floor, playerLevel, pos);
            level.addEnemy(c);
        }
    }

    protected Position findValidPosition(Level level) {
        return LevelUtils.findValidPosition(level);
    }

    protected abstract Character generateEnemy(int floor, int playerLevel, Position pos);

}
