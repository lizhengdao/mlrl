/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.peritasoft.mlrl.ai.PathFinder;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.Race;
import com.peritasoft.mlrl.effects.LifeObj;
import com.peritasoft.mlrl.effects.Projectile;
import com.peritasoft.mlrl.effects.ProjectileType;
import com.peritasoft.mlrl.events.Log;
import com.peritasoft.mlrl.item.Item;

import java.util.ArrayList;
import java.util.Iterator;

public class Level implements PathFinder.TraverseableMap {
    private final Cell[][] cells;
    private final int width;
    private final int height;
    private final LevelType type;
    private final Position startingPosition;
    private final ArrayList<LifeObj> lifeObjs;
    private Position staircaseDownPosition;
    private Projectile projectile;
    private final ArrayList<LifeObj> lifeObjToAdd;

    public Level(Tile[][] tiles, Position startingPosition, LevelType type) {
        this.height = tiles.length;
        this.width = tiles[0].length;
        this.type = type;
        this.startingPosition = startingPosition;
        lifeObjs = new ArrayList<>();
        lifeObjToAdd = new ArrayList<>();
        this.cells = new Cell[height][width];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                Tile tile = tiles[y][x];
                final WallJoint wallJoint = tile != Tile.WALL ?
                        WallJoint.NONE :
                        WallJoint.find(y < height - 1 && tiles[y + 1][x] == Tile.WALL,
                                x > 0 && tiles[y][x - 1] == Tile.WALL,
                                y > 0 && tiles[y - 1][x] == Tile.WALL,
                                x < width - 1 && tiles[y][x + 1] == Tile.WALL);
                cells[y][x] = new Cell(tile, wallJoint);
                if (tile == Tile.STAIRCASEDOWN) {
                    if (staircaseDownPosition != null) {
                        throw new IllegalArgumentException("This level has two staircases down");
                    }
                    staircaseDownPosition = new Position(x, y);
                }
            }
        }
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public Projectile getProjectile() {
        return projectile;
    }

    public void setProjectile(Projectile projectile) {
        this.projectile = projectile;
    }

    public LevelType getType() {
        return type;
    }

    public ArrayList<LifeObj> getLifeObjs() {
        return lifeObjs;
    }

    public Cell getCell(int x, int y) {
        if (x < 0 || x >= width || y < 0 || y >= height) {
            return Cell.BOUND;
        } else {
            return cells[y][x];
        }
    }

    public Cell getCell(Position pos) {
        return getCell(pos.getX(), pos.getY());
    }

    public Position getStartingPosition() {
        return startingPosition;
    }

    public Position getPosStaircaseDown() {
        return staircaseDownPosition;
    }

    public Iterable<Character> getEnemies() {
        return new EnemyIterable(this);
    }

    public void dropItem(Item item, Position pos) {
        int dropY = pos.getY();
        int dropX = pos.getX();
        Cell cell = getCell(dropX, dropY);
        while (cell.hasItem()) {
            Direction dir = Direction.atRandom();
            Cell nextCell = getCell(dropX + dir.x, dropY + dir.y);
            if (nextCell.isWalkable()) {
                dropX = dropX + dir.x;
                dropY = dropY + dir.y;
                cell = nextCell;
            }
        }
        cell.setItem(item);
    }

    public Item getItem(int x, int y) {
        return getCell(x, y).getItem();
    }

    public void removeProp(Position pos) {
        removeProp(pos.getX(), pos.getY());
    }

    public void removeProp(int x, int y) {
        getCell(x, y).removeProp();
    }

    public boolean hasNoObstacles(Position initialPos, Position finalPos) {
        return Geometry.walkLine(initialPos, finalPos, new Geometry.LineWalker() {
            @Override
            public boolean walk(Position p) {
                return !getCell(p.getX(), p.getY()).blocksSight();
            }
        });
    }

    public boolean isWalkable(int x, int y) {
        return getCell(x, y).isWalkable();
    }

    public boolean isWalkable(Position pos) {
        return isWalkable(pos.getX(), pos.getY());
    }

    @Override
    public boolean isTraverseable(int x, int y) {
        return getCell(x, y).isTravesable();
    }

    public boolean isGround(int x, int y) {
        return getCell(x, y).tile == Tile.GROUND;
    }

    public boolean isGround(Position pos) {
        return isGround(pos.getX(), pos.getY());
    }

    public void see(final int positionX, final int positionY, final int sightRadius) {
        final int top = positionY - sightRadius;
        final int left = positionX - sightRadius;
        final int bottom = positionY + sightRadius;
        final int right = positionX + sightRadius;
        final Position start = new Position(positionX, positionY);
        final Position end = new Position(0, 0);
        final Geometry.LineWalker markTiles = new Geometry.LineWalker() {
            @Override
            public boolean walk(Position p) {
                Cell cell = getCell(p.getX(), p.getY());
                Quadrant q;
                if (p.getX() > positionX) {
                    q = p.getY() >= positionY ? Quadrant.SE : Quadrant.NE;
                } else {
                    q = p.getY() >= positionY ? Quadrant.SW : Quadrant.NW;
                }
                if (!cell.blocksLight()) {
                    Cell c1 = getCell(p.getX(), p.getY() + q.y);
                    Cell c2 = getCell(p.getX() + q.x, p.getY());

                    if (c1.blocksLight() && c2.blocksLight()) {
                        cell.setFovRange(1f);
                        return false;
                    }
                }
                cell.computeVisibility(positionX, positionY, p.getX(), p.getY(), sightRadius);
                return !cell.blocksLight();
            }
        };
        getCell(positionX, positionY).setFovRange(0.0f); // Geometry.walkLines never calls walker for first cell
        for (int y = top; y <= bottom; y++) {
            end.setY(y);
            end.setX(left);
            Geometry.walkLine(start, end, markTiles);
            end.setX(right);
            Geometry.walkLine(start, end, markTiles);
        }
        for (int x = left; x <= right; x++) {
            end.setX(x);
            end.setY(top);
            Geometry.walkLine(start, end, markTiles);
            end.setY(bottom);
            Geometry.walkLine(start, end, markTiles);
        }
        for (int y = top; y <= bottom; y++) {
            for (int x = left; x <= right; x++) {
                Quadrant q;
                if (x > positionX) {
                    q = y >= positionY ? Quadrant.SE : Quadrant.NE;
                } else {
                    q = y >= positionY ? Quadrant.SW : Quadrant.NW;
                }

                Cell cell = getCell(x, y);
                if (cell.isInFOV() && cell.blocksLight()) {
                    Cell c1 = getCell(x, y + q.y);
                    Cell c2 = getCell(x + q.x, y);
                    Cell c3 = getCell(x + q.x, y + q.y);

                    if ((c1.isInFOV() && !c1.blocksLight()) ||
                            (c2.isInFOV() && !c2.blocksLight()) ||
                            (c3.isInFOV() && !c3.blocksLight())) {
                        cell.computeVisibility(positionX, positionY, x, y, sightRadius);
                    }
                }
            }
        }
    }

    private void clearFov() {
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                getCell(x, y).setFovRange(1f);
            }
        }
    }

    public void addEnemy(Character enemy) {
        Cell cell = getCell(enemy.getPosition());
        if (!cell.isWalkable()) {
            throw new IllegalArgumentException("Enemies can not be added to non-walkable cells");
        }
        if (cell.hasCharacter()) {
            throw new IllegalArgumentException("Can not have two characters in the same cell");
        }
        cell.setCharacter(enemy);
    }

    public void removeEnemy(Character enemy) {
        Cell cell = getCell(enemy.getPosition());
        cell.removeCharacter();
    }

    public boolean hasCharacterIn(int x, int y) {
        return getCell(x, y).hasCharacter();
    }

    public boolean hasCharacterIn(Position pos) {
        return hasCharacterIn(pos.getX(), pos.getY());
    }

    public boolean hasEnemies() {
        return getEnemies().iterator().hasNext();
    }

    public void move(Character character, Position from, int positionX, int positionY) {
        getCell(from).removeCharacter();
        getCell(positionX, positionY).setCharacter(character);
    }

    public void putStaircaseDown(Position position) {
        cells[position.getY()][position.getX()].setTile(Tile.STAIRCASEDOWN);
        staircaseDownPosition = position;
    }

    public void logEnter(Log log) {
        // Nothing to do
    }

    public boolean canDescendToNextFloor(Position position) {
        return position.equals(getPosStaircaseDown());
    }

    public boolean canAscendToPreviousFloor(Position position) {
        return position.equals(getStartingPosition());
    }

    public boolean add(LifeObj obj) {
        return lifeObjToAdd.add(obj);
    }

    public void update(float delta) {
        getLifeObjs().addAll(lifeObjToAdd);
        lifeObjToAdd.clear();
        clearFov();
        setFov();
    }

    private void setFov() {
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                Cell c = getCell(x, y);
                if (c.hasCharacter() && c.getCharacter().getRace() == Race.HUMAN) {
                    see(x, y, c.getCharacter().getSightRadius());
                }
                if (c.hasProp()) {
                    switch (c.getProp().getType()) {
                        case TORCH:
                        case CANDLES:
                        case LIGHT_POINT:
                            see(x, y - 1, 4);
                            break;
                    }
                }
                if (c.tile == Tile.LAVA) {
                    see(x, y, 2);
                }
            }
        }
        for (LifeObj o : getLifeObjs()) {
            switch (o.getType()) {
                case FIRE:
                case LIGHTNING:
                    see(o.getPositionX(), o.getPositionY(), 2);
                    break;
            }
        }
        Projectile p = getProjectile();
        if (p != null) {
            if (p.getProjectileType() == ProjectileType.FIREBALL) {
                see(p.getPosition().getX(), p.getPosition().getY(), 2);
            }
        }
    }

    public boolean hasFov() {
        return true;
    }

    public boolean blockSight(int x, int y) {
        return getCell(x, y).blocksSight();
    }

    public static class EnemyIterable implements Iterable<Character> {
        private final Level level;

        public EnemyIterable(Level level) {
            this.level = level;
        }

        @SuppressWarnings("NullableProblems")
        @Override
        public Iterator<Character> iterator() {
            return new EnemyIterator(level);
        }
    }

    public static class EnemyIterator implements Iterator<Character> {
        private final Level level;
        private Character nextCharacter;

        public EnemyIterator(Level level) {
            this.level = level;
            this.nextCharacter = findNext();
        }

        @Override
        public boolean hasNext() {
            return nextCharacter != null;
        }

        @Override
        public Character next() {
            Character character = nextCharacter;
            nextCharacter = character == null ? null : findNext();
            return character;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("remove");
        }

        private Character findNext() {
            final int startY = nextCharacter == null ? 0 : nextCharacter.getPositionY();
            for (int y = startY; y < level.getHeight(); y++) {
                final int startX = y > startY || nextCharacter == null ? 0 : nextCharacter.getPositionX() + 1;
                for (int x = startX; x < level.getWidth(); x++) {
                    Cell cell = level.getCell(x, y);
                    if (cell.hasCharacter()) {
                        Character character = cell.getCharacter();
                        if (character.getRace() != Race.HUMAN) {
                            return character;
                        }
                    }
                }
            }
            return null;
        }
    }

    private enum Quadrant {
        NW(1, 1),
        SW(1, -1),
        SE(-1, -1),
        NE(-1, 1);

        final int x;
        final int y;

        Quadrant(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
