/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.PlayerHero;

import java.util.Arrays;

public class CenterLevelGenerator implements LevelGenerator {
    private static final int MIN_ROOM_WIDTH = 8;
    private static final int MAX_ROOM_WIDTH = 12;
    private static final int MIN_ROOM_HEIGHT = 8;
    private static final int MAX_ROOM_HEIGHT = 12;

    private final int nRooms;
    private final int width;
    private final int height;
    private final LevelType levelType;

    public CenterLevelGenerator(int nRooms, int width, int height, LevelType levelType) {
        this.nRooms = nRooms;
        this.width = width;
        this.height = height;
        this.levelType = levelType;
    }

    public int getNumRooms() {
        return nRooms;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        Tile[][] tiles = new Tile[height][width];
        fillWithWalls(tiles);
        Position[] rooms = new Position[nRooms];
        int realRooms = 0;
        for (int i = 0; i < nRooms; i++) {
            for (int tries = 1; tries <= 1000; tries++) {
                int x = MathUtils.random(width - 1);
                int y = MathUtils.random(height - 1);
                if (createRoom(tiles, x, y)) {
                    rooms[realRooms] = new Position(x, y);
                    realRooms++;
                    break;
                }
            }
        }
        int nCorridors = realRooms + 1;
        int[][] roomPairs = new int[nCorridors][2];
        int num = 0;
        for (int i = 0; i < nCorridors; i++) {
            roomPairs[i][0] = num;
            roomPairs[i][1] = -1;
            int tries = nRooms * 100;
            while (true) {
                int num2 = MathUtils.random(0, realRooms - 1);
                if (num2 == num) continue;
                boolean repeated = false;
                for (int k = 0; k <= i; k++) {
                    if ((roomPairs[k][0] == num && roomPairs[k][1] == num2) || (roomPairs[k][0] == num2 && roomPairs[k][1] == num)) {
                        repeated = true;
                        break;
                    }
                }
                if (repeated) {
                    tries--;
                    if (tries == 0) {
                        num = (num + 1) % realRooms;
                        tries = nRooms * 100;
                    }
                    continue;
                }
                roomPairs[i][1] = num2;
                break;
            }
            num++;
            if (num == realRooms) {
                num = 0;
            }
        }

        for (int i = 0; i < nCorridors; i++) {
            Position r1 = rooms[roomPairs[i][0]];
            Position r2 = rooms[roomPairs[i][1]];
            createCorridor(tiles, r1, r2);
        }

        Position startingPosition = LevelUtils.putStaircases(tiles, upStaircase, true);
        removeBoundaries(tiles);
        return new Level(tiles, startingPosition, levelType);
    }

    private void fillWithWalls(Tile[][] tiles) {
        for (Tile[] row : tiles) {
            Arrays.fill(row, Tile.WALL);
        }
    }

    private boolean createRoom(Tile[][] tiles, int centerX, int centerY) {
        int roomWidth = (int) Math.floor(MathUtils.random() * (MAX_ROOM_WIDTH - MIN_ROOM_WIDTH + 1) + MIN_ROOM_WIDTH);
        int roomHeight = (int) Math.floor(MathUtils.random() * (MAX_ROOM_HEIGHT - MIN_ROOM_HEIGHT + 1) + MIN_ROOM_HEIGHT);
        //check if room fits
        for (int y = 0; y < roomHeight; y++) {
            for (int x = 0; x < roomWidth; x++) {
                if (getTile(tiles, x + centerX - (roomWidth / 2), y + centerY - (roomHeight / 2)) != Tile.WALL) {
                    return false;
                }
            }
        }
        //carve the room
        for (int y = 0; y < roomHeight; y++) {
            for (int x = 0; x < roomWidth; x++) {
                if (y == 0 || x == 0 || y == roomHeight - 1 || x == roomWidth - 1) {
                    tiles[y + centerY - (roomHeight / 2)][x + centerX - (roomWidth / 2)] = Tile.WALL;
                } else {
                    tiles[y + centerY - (roomHeight / 2)][x + centerX - (roomWidth / 2)] = Tile.GROUND;
                }
            }
        }
        return true;
    }

    private Tile getTile(Tile[][] tiles, int x, int y) {
        if (x < 0 || x >= tiles[0].length || y < 0 || y >= tiles.length) {
            return Tile.BOUND;
        } else {
            return tiles[y][x];
        }
    }

    private void createCorridor(Tile[][] tiles, Position r1, Position r2) {
        Position minX = r1.getX() < r2.getX() ? r1 : r2;
        Position maxX = r1.getX() < r2.getX() ? r2 : r1;
        for (int x = minX.getX(); x <= maxX.getX(); x++) {
            tiles[minX.getY()][x] = Tile.GROUND;
        }
        int startY = Math.min(maxX.getY(), minX.getY());
        int endY = Math.max(maxX.getY(), minX.getY());
        for (int y = startY; y <= endY; y++) {
            tiles[y][maxX.getX()] = Tile.GROUND;
        }
    }

    private void removeBoundaries(Tile[][] tiles) {
        final int height = tiles.length;
        final int width = tiles[0].length;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (tiles[y][x] == Tile.WALL && LevelUtils.countSurroundingWallsFloors(tiles, x, y, 1)[1] == 0) {
                    tiles[y][x] = Tile.BLANK;
                }
            }
        }
    }

}
