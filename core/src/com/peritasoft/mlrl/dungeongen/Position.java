/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

public class Position {
    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position(Position position) {
        this.x = position.x;
        this.y = position.y;
    }

    public Position(Position position, Direction direction) {
        this.x = position.x + direction.x;
        this.y = position.y + direction.y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x &&
                y == position.y;
    }

    public boolean equals(int x, int y) {
        return this.x == x &&
                this.y == y;
    }

    /*
    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
*/
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void set(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void set(Position newPos) {
        set(newPos.getX(), newPos.getY());
    }

    public static double distance(int x1, int y1, int x2, int y2) {
        final double dx = x1 - x2;
        final double dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public int distance(Position to) {
        return (int) distance(getX(), getY(), to.getX(), to.getY());
    }

    public Direction directionTo(Position pos) {
        if (pos.y > y) {
            return Direction.NORTH;
        } else if (pos.x < x) {
            return Direction.WEST;
        } else if (pos.y < y) {
            return Direction.SOUTH;
        } else if (pos.x > x) {
            return Direction.EAST;
        }
        return Direction.NONE;
    }
}
