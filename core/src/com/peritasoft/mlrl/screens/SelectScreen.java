/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.characters.Demography;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.characters.Sex;
import com.peritasoft.mlrl.easing.EaseNone;
import com.peritasoft.mlrl.easing.EaseOutBack;
import com.peritasoft.mlrl.easing.Easing;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.InventoryRandomizer;
import com.peritasoft.mlrl.render.SelectRenderer;
import com.peritasoft.mlrl.render.TitleBackground;
import com.peritasoft.mlrl.scores.LocalScoreRecorder;

import java.util.EnumSet;
import java.util.Set;

public class SelectScreen extends MlrlScreenAdapter implements GestureDetector.GestureListener {
    private final MyLittleRogueLike game;
    private final GestureDetector gestureDetector;
    private final Choice[][] choices;
    private final SelectRenderer renderer;
    private int selected;
    private final Vector2 tapPoint;
    private Sex sex;
    private final Set<Demography> unlocked;
    private TitleBackground background;
    private final Matrix4 transformation;
    private Easing easing;
    private Runnable fadingTo;
    private  final boolean isEndless;

    public SelectScreen(MyLittleRogueLike game, TitleBackground background, boolean isEndless) {
        this.game = game;
        this.background = background;
        this.isEndless = isEndless;
        background.setSelectTitle();
        unlocked = EnumSet.of(Demography.HERO_WARRIOR);
        if (game.isArcherUnlocked()) {
            unlocked.add(Demography.HERO_ARCHER);
        }
        if (game.isMageUnlocked()) {
            unlocked.add(Demography.HERO_MAGE);
        }

        this.sex = game.getLastSexChoice(Sex.MALE);
        this.selected = game.getLastPlayerChoice(1);
        this.renderer = new SelectRenderer(game.getSmallFont(), this.selected);
        this.transformation = new Matrix4();
        this.easing = new EaseOutBack(-MyLittleRogueLike.SCREENHEIGHT, 0f, 0f);
        final LocalScoreRecorder scores = game.getLocalScoreRecorder();
        choices = new Choice[][]{
                {
                        new Choice(scores, Demography.HERO_WARRIOR, Sex.FEMALE),
                        new Choice(scores, Demography.HERO_ARCHER, Sex.FEMALE),
                        new Choice(scores, Demography.HERO_MAGE, Sex.FEMALE)
                },
                {
                        new Choice(scores, Demography.HERO_WARRIOR, Sex.MALE),
                        new Choice(scores, Demography.HERO_ARCHER, Sex.MALE),
                        new Choice(scores, Demography.HERO_MAGE, Sex.MALE)
                },
        };

        rerollAllChoicesItems();
        gestureDetector = new GestureDetector(this);
        tapPoint = new Vector2(0, 0);
    }

    @Override
    public void render(float delta) {
        update(delta);
        if (isFading() && isFadingComplete()) {
            game.getBatch().setColor(Color.WHITE);
            fadingTo.run();
        } else {
            draw(game.getBatch());
        }
    }

    private void update(float delta) {
        background.update(delta);
        easing.update(delta);
    }

    private void draw(Batch batch) {
        background.clear();
        batch.begin();
        batch.setColor(1f, 1f, 1f, 1f);
        background.draw(batch);
        batch.end();
        batch.begin();
        final float alpha = isFading() ? easing.getValue() : 1f;
        if (isFading()) {
            batch.setColor(1f, 1f, 1f, alpha);
        } else {
            transformation.translate(0, easing.getValue(), 0);
            batch.setTransformMatrix(transformation);
        }
        renderer.render(batch, getChoices(), unlocked, sex, selected, alpha);
        batch.end();
        transformation.idt();
        batch.setTransformMatrix(transformation);
    }

    private void rerollAllChoicesItems() {
        for (Sex sex : Sex.values()) {
            for (Choice choice : choices[sex.ordinal()]) {
                choice.rerollItems();
            }
        }
    }

    private Choice[] getChoices() {
        return choices[sex.ordinal()];
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.R:
                rerollAllChoicesItems();
                return true;
            case Input.Keys.F:
                sex = Sex.FEMALE;
                return true;
            case Input.Keys.M:
                sex = Sex.MALE;
                return true;
            case Input.Keys.LEFT:
                if (selected > 0) {
                    selected--;
                }
                return true;
            case Input.Keys.RIGHT:
                if (selected < getChoices().length - 1) {
                    selected++;
                }
                return true;
            case Input.Keys.ENTER:
            case Input.Keys.SPACE:
                nextScreen();
                return true;
        }
        return false;
    }

    private void nextScreen() {
        if (isSelectedUnblocked() && !isFading()) {
            fadeTo(new Runnable() {
                @Override
                public void run() {
                    dispose();
                    game.recordPlayerChoice(selected, sex);
                    game.setScreen(new PlayScreen(game, getChoices()[selected].getPlayer(), isEndless));
                }
            });
        }
    }

    private void fadeTo(Runnable runnable) {
        if (isFading()) return;
        background.hideTitle();
        easing = new EaseNone(1f, 0f, 0f);
        fadingTo = runnable;
    }

    private boolean isFading() {
        return fadingTo != null;
    }

    private boolean isFadingComplete() {
        return easing.isDone();
    }

    private boolean isSelectedUnblocked() {
        final Choice choice = getChoices()[selected];
        return unlocked.contains(choice.getDemography());
    }

    @Override
    public boolean touchDown(Vector2 touchPoint, int pointer, int button) {
        return gestureDetector.touchDown(touchPoint.x, touchPoint.y, pointer, button);
    }

    @Override
    public boolean touchUp(Vector2 touchPoint, int pointer, int button) {
        return gestureDetector.touchUp(touchPoint.x, touchPoint.y, pointer, button);
    }

    @Override
    public boolean touchDragged(Vector2 touchPoint, int pointer) {
        return gestureDetector.touchDragged(touchPoint.x, touchPoint.y, pointer);
    }

    @Override
    public void dispose() {
        renderer.dispose();
        if (background != null) {
            background.dispose();
        }
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        if (isFading()) return false;
        if (button != Input.Buttons.LEFT) return false;
        tapPoint.set(x, y);
        if (renderer.rerollClicked(tapPoint)) {
            rerollAllChoicesItems();
            return true;
        }
        if (renderer.nextClicked(tapPoint)) {
            return keyDown(Input.Keys.RIGHT);
        }
        if (renderer.prevClicked(tapPoint)) {
            return keyDown(Input.Keys.LEFT);
        }
        if (renderer.femaleClicked(tapPoint)) {
            return keyDown(Input.Keys.F);
        }
        if (renderer.maleClicked(tapPoint)) {
            return keyDown(Input.Keys.M);
        }
        if (renderer.choiceClicked(tapPoint)) {
            return keyDown(Input.Keys.ENTER);
        }
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        if (Math.abs(velocityX) > Math.abs(velocityY)) {
            return keyDown(velocityX > 0 ? Input.Keys.LEFT : Input.Keys.RIGHT);
        }
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }

    public static class Choice {
        private final PlayerHero player;
        private final String killerName;
        private final int maxFloor;

        public Choice(LocalScoreRecorder scores, Demography demography, Sex sex) {
            player = new PlayerHero(demography,
                    sex,
                    1,
                    PlayerHero.initialStr(demography),
                    PlayerHero.initialDex(demography),
                    PlayerHero.initialWis(demography),
                    PlayerHero.initialCon(demography)
            );
            maxFloor = scores.getMaxFloor(player);
            killerName = scores.getKillerName(player);
        }

        public void rerollItems() {
            InventoryRandomizer.randomizeInventory(getInventory(), getDemography());
            getPlayer().equip(getInventory().get(0));
        }

        public Demography getDemography() {
            return getPlayer().getDemography();
        }

        public Inventory getInventory() {
            return getPlayer().getInventory();
        }

        public PlayerHero getPlayer() {
            return player;
        }

        public int getMaxFloor() {
            return maxFloor;
        }

        public String getKillerName() {
            return killerName;
        }
    }
}
