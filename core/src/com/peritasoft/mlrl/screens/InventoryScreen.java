/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.render.InventoryRenderer;

class InventoryScreen extends MlrlScreenAdapter {
    private final MyLittleRogueLike game;
    private final PlayScreen playScreen;
    private final PlayerHero player;
    private final InventoryRenderer renderer;
    private int selected = 0;
    private boolean updateEnemies;

    InventoryScreen(MyLittleRogueLike game, PlayerHero player, PlayScreen playScreen) {
        this.game = game;
        this.playScreen = playScreen;
        renderer = new InventoryRenderer(game.getBatch(), game.getSmallFont());
        this.player = player;
        this.updateEnemies = false;
    }

    @Override
    public void render(float delta) {
        playScreen.render(0);
        game.getBatch().begin();
        renderer.render(player, player.getInventory().get(selected));
        game.getBatch().end();
    }

    private void goBack() {
        game.setScreen(playScreen);
        GameEvent.closeInventory();
        if (updateEnemies) playScreen.getDungeon().updateEnemies();
    }

    @Override
    public void resume() {
        playScreen.resume();
    }

    @Override
    public void pause() {
        playScreen.pause();
    }

    @Override
    public void dispose() {
        playScreen.dispose();
        renderer.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.ESCAPE:
                goBack();
                return true;
            case Input.Keys.DOWN:
                selected = Math.min(selected + 5, player.getInventory().size() - 1);
                return true;
            case Input.Keys.UP:
                selected = Math.max(selected - 5, 0);
                return true;
            case Input.Keys.RIGHT:
                selected = Math.min(selected + 1, player.getInventory().size() - 1);
                return true;
            case Input.Keys.LEFT:
                selected = Math.max(selected - 1, 0);
                return true;
            case Input.Keys.PAGE_DOWN:
                selected = Math.min(selected + InventoryRenderer.ITEMS_PER_PAGE, player.getInventory().size() - 1);
                return true;
            case Input.Keys.PAGE_UP:
                selected = Math.max(selected - InventoryRenderer.ITEMS_PER_PAGE, 0);
                return true;
            case Input.Keys.E:
                equipItem();
                return true;
            case Input.Keys.U:
                useItem();
                return true;
            case Input.Keys.D:
                dropItem();
                return true;
        }
        return false;
    }

    private void dropItem() {
        Item item = player.getInventory().get(selected);
        if (item != null) {
            playScreen.getDungeon().getCurrentLevel().dropItem(item, player.getPosition());
            player.unequip(item);
            selected = player.getInventory().remove(selected);
            updateEnemies = true;
        }
    }

    private void useItem() {
        Item usable = player.getInventory().get(selected);
        if (player.use(usable, playScreen.getDungeon().getCurrentLevel())) {
            if (!player.isAiming()) {
                player.getInventory().remove(selected);
                playScreen.getDungeon().update(Gdx.graphics.getDeltaTime());
                playScreen.getDungeon().updateEnemies();
            }
            selected--;
            game.setScreen(playScreen);
        }
    }

    private void equipItem() {
        Item equipable = player.getInventory().get(selected);
        player.equip(equipable);
        updateEnemies = true;
    }

    @Override
    public boolean touchDown(Vector2 touchPoint, int pointer, int button) {
        if (button != Input.Buttons.LEFT || pointer > 0) return false;
        int pressed = renderer.getCellPressed(touchPoint);
        if (pressed != -1 && pressed < player.getInventory().size()) {
            selected = pressed;
            return true;
        }
        if (renderer.buttonPressed(touchPoint)) {
            Item itemSelected = player.getInventory().get(selected);
            if (itemSelected != null) {
                if (itemSelected.isEquipable()) {
                    equipItem();
                } else {
                    useItem();
                }
                return true;
            }
        }
        if (renderer.buttonDropPressed(touchPoint)) {
            dropItem();
        }
        if (playScreen.buttonInventoryPressed(touchPoint) || renderer.buttonClosePressed(touchPoint)) {
            goBack();
            return true;
        }
        return false;
    }
}
