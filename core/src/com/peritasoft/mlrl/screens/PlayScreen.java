/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.crashinvaders.vfx.VfxManager;
import com.crashinvaders.vfx.effects.WaterDistortionEffect;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.*;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.events.Log;
import com.peritasoft.mlrl.events.MusicManager;
import com.peritasoft.mlrl.events.SfxManager;
import com.peritasoft.mlrl.render.GameRenderer;
import com.peritasoft.mlrl.render.GuiRenderer;
import com.peritasoft.mlrl.render.LogRenderer;

public class PlayScreen extends MlrlScreenAdapter {
    private GameRenderer renderer;
    private final GuiRenderer guiRenderer;
    private Dungeon dungeon;
    private MyLittleRogueLike game;
    private MusicManager bgMusic;
    private Log log;
    private SfxManager sfx;
    private final PlayerHero player;
    private final LogRenderer logRenderer;
    private boolean inInventory;
    private boolean inSettings;
    private boolean skipping;
    private boolean ownRenderer = false;
    private boolean newGame;
    private final long seed;
    private boolean deserializedGame;
    private final VfxManager vfxManager;
    private final WaterDistortionEffect waterEffect;

    public PlayScreen(MyLittleRogueLike game, PlayerHero player, boolean isEndless) {
        this(game, player, 30, 30, 1, TimeUtils.millis(), new GameRenderer(game.getBatch()), isEndless);
        ownRenderer = true;
    }

    public PlayScreen(MyLittleRogueLike game, PlayerHero player, int floorWidth, int floorHeight, int startingFloor, long seed, GameRenderer renderer, boolean isEndless) {
        this(
                game
                , player
                , buildDungeon(player, floorWidth, floorHeight, startingFloor, isEndless)
                , new Log(player)
                , renderer
                , seed
                , true
        );
    }

    public PlayScreen(MyLittleRogueLike game, PlayerHero player, Dungeon dungeon, Log log, long seed, boolean newGame) {
        this(game, player, dungeon, log, new GameRenderer(game.getBatch()), seed, newGame);
        ownRenderer = true;
    }

    public PlayScreen(MyLittleRogueLike game, PlayerHero player, Dungeon dungeon, Log log, GameRenderer renderer, long seed, boolean newGame) {
        this.game = game;
        this.newGame = newGame;
        this.player = player;
        this.dungeon = dungeon;
        this.log = log;

        MathUtils.random.setSeed(seed);
        this.seed = seed;

        sfx = new SfxManager();
        bgMusic = new MusicManager();

        guiRenderer = new GuiRenderer(game.getBatch(), game.getSmallFont());
        this.renderer = renderer;
        logRenderer = new LogRenderer(game.getBatch(), game.getSmallFont(), 4, MyLittleRogueLike.SCREENHEIGHT + 8);
        if (!game.isTilesetGraphical()) {
            toggleTileset();
        }
        if (!game.isMusicOn()) {
            toggleMusic();
        }

        vfxManager = new VfxManager(Pixmap.Format.RGB888);
        waterEffect = new WaterDistortionEffect(10f, 10f);
        vfxManager.addEffect(waterEffect);
    }

    protected static Dungeon buildDungeon(PlayerHero player, int floorWidth, int floorHeight, int startingFloor, boolean isEndless) {
        LevelGenerator levelGenerator;
        if (isEndless) {
            levelGenerator = LevelGeneratorBuilder.buildEndlessLevelGenerator(floorWidth, floorHeight);
        } else {
            levelGenerator = LevelGeneratorBuilder.buildLevelGenerator(floorWidth, floorHeight);
        }
        LevelGenerator generator = new MemoizedLevelGenerator(levelGenerator, floorWidth, floorHeight, isEndless);
        return new Dungeon(startingFloor, generator, player);
    }

    @Override
    public void show() {
        if (newGame) {
            newGame = false;
            GameEvent.newGame(seed);
        } else if (deserializedGame) {
            deserializedGame = false;
            bgMusic.changedFloor(getDungeon().getCurrentFloor(), getCurrentLevel());
            GameEvent.resumeGame(seed);
        }
        inInventory = false;
        inSettings = false;
    }

    public void deserialize() {
        deserializedGame = true;
    }

    public Dungeon getDungeon() {
        return dungeon;
    }

    public Log getLog() {
        return log;
    }

    public PlayerHero getPlayer() {
        return player;
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        vfxManager.resize(width, height);
    }

    @Override
    public void render(float delta) {
        boolean distort = dungeon.shouldDistort();
        if (distort) {
            vfxManager.update(delta);
            vfxManager.cleanUpBuffers();
            vfxManager.beginInputCapture();
        }
        game.getBatch().begin();
        renderer.render(getCurrentLevel(), player, dungeon.getProjectile(), delta);
        if (distort) {
            game.getBatch().end();
            vfxManager.endInputCapture();
            vfxManager.applyEffects();
            vfxManager.renderToScreen();
            game.getBatch().begin();
        }
        guiRenderer.render(dungeon.getCurrentFloor(), player, player.isAiming(), player.isDead(), skipping, inInventory, inSettings, shouldShowPad(), isPadAtRight());
        logRenderer.render(log);
        game.getBatch().end();
        update(delta);
    }

    @Override
    public void dispose() {
        vfxManager.dispose();
        waterEffect.dispose();
        if (sfx != null) sfx.dispose();
        log.dispose();
        guiRenderer.dispose();
        if (ownRenderer) {
            renderer.dispose();
        }
        dungeon.dispose();
        if (bgMusic != null) bgMusic.dispose();
    }

    private void update(float delta) {
        dungeon.update(delta);
        if (dungeon.isEndGame()) {
            game.clearOngoingRun();
            game.setScreen(new EndScreen(game, bgMusic, sfx));
            bgMusic = null;
            sfx = null;
            dispose();
            return;
        }
        Direction dir = player.followPath(delta);
        if (dir != null) {
            switch (dir) {
                case NORTH:
                    dungeon.update(PlayerHero.Key.UP);
                    break;
                case WEST:
                    dungeon.update(PlayerHero.Key.LEFT);
                    break;
                case SOUTH:
                    dungeon.update(PlayerHero.Key.DOWN);
                    break;
                case EAST:
                    dungeon.update(PlayerHero.Key.RIGHT);
                    break;
            }
        }
        skipping = false;
    }

    private void restartGame() {
        if (player.isDead()) {
            game.clearOngoingRun();
        } else {
            GameEvent.pauseGame();
            game.saveOngoingRun(this);
        }
        game.setScreen(new TitleScreen(game));
        dispose();
    }

    private void goToInventory() {
        if (!player.isPetrified()) {
            player.stopAiming();
            game.setScreen(new InventoryScreen(game, player, this));
            GameEvent.openInventory();
            inInventory = true;
        }
    }

    @Override
    public void pause() {
        bgMusic.pause();
        if (!player.isDead()) {
            GameEvent.pauseGame();
            game.saveOngoingRun(this);
        }
    }

    @Override
    public void resume() {
        bgMusic.play();
        GameEvent.resumeGame(seed);
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.PAGE_DOWN:
                dungeon.nextFloor();
                return true;
            case Input.Keys.PAGE_UP:
                dungeon.previousFloor();
                return true;
            case Input.Keys.TAB:
                toggleTileset();
                return true;
            case Input.Keys.I:
                if (!player.isDead()) {
                    goToInventory();
                    return true;
                }
                break;
            case Input.Keys.ESCAPE:
                if (player.isAiming()) {
                    dungeon.update(PlayerHero.Key.CANCEL);
                } else {
                    showMenu();
                }
                return true;
            case Input.Keys.ENTER:
                if (player.isDead()) {
                    restartGame();
                    return true;
                }
                break;
            case Input.Keys.UP:
                dungeon.update(PlayerHero.Key.UP);
                return true;
            case Input.Keys.LEFT:
                dungeon.update(PlayerHero.Key.LEFT);
                return true;
            case Input.Keys.DOWN:
                dungeon.update(PlayerHero.Key.DOWN);
                return true;
            case Input.Keys.RIGHT:
                dungeon.update(PlayerHero.Key.RIGHT);
                return true;
            case Input.Keys.F:
                dungeon.update(PlayerHero.Key.FIRE);
                return true;
            case Input.Keys.L:
                dungeon.update(PlayerHero.Key.LOOK);
                return true;
        }
        return false;
    }

    private void showMenu() {
        game.setScreen(new MenuScreen(game, this));
        inSettings = true;
    }

    @Override
    public boolean touchDown(Vector2 touchPoint, int pointer, int button) {
        if (button != Input.Buttons.LEFT || pointer > 0) return false;
        if (player.isDead()) {
            if (guiRenderer.deadPanelPressed(touchPoint)) {
                restartGame();
            }
        } else {
            if (shouldShowPad()) {
                Direction padDir = guiRenderer.padPressed(touchPoint);
                if (padDir != Direction.NONE) {
                    switch (padDir) {
                        case NORTH:
                            keyDown(Input.Keys.UP);
                            break;
                        case EAST:
                            keyDown(Input.Keys.RIGHT);
                            break;
                        case SOUTH:
                            keyDown(Input.Keys.DOWN);
                            break;
                        case WEST:
                            keyDown(Input.Keys.LEFT);
                            break;
                    }
                    return true;
                }
            }
            if (buttonInventoryPressed(touchPoint)) {
                goToInventory();
            } else if (guiRenderer.buttonSkipPressed(touchPoint, player.isAiming())) {
                skipping = true;
                dungeon.updateEnemies();
            } else if (guiRenderer.buttonAimPressed(touchPoint)) {
                if (player.isAiming()) player.stopAiming();
                else if (player.canFireWeapon()) player.aim(player.shootWeapon(), getCurrentLevel());
            } else if (player.isAiming() && shouldShowPad() && guiRenderer.buttonShootPressed(touchPoint)) {
                keyDown(Input.Keys.F);
            } else if (buttonConfigPressed(touchPoint)) {
                showMenu();
            } else {
                if (shouldShowPad()) return false;
                Position position = getLevelPosition(touchPoint.x, touchPoint.y);
                if (player.isAiming()) {
                    player.aimAt(getCurrentLevel(), position);
                } else {
                    player.moveTo(getCurrentLevel(), position);
                }
            }
        }
        return true;
    }

    @Override
    public boolean touchUp(Vector2 touchPoint, int pointer, int button) {
        if (button != Input.Buttons.LEFT || pointer > 0) return false;
        return guiRenderer.resetdPad();
    }

    public Position getLevelPosition(float x, float y) {
        return renderer.unproject(getCurrentLevel(), player, x, y);
    }

    public Level getCurrentLevel() {
        return dungeon.getCurrentLevel();
    }

    public boolean buttonConfigPressed(Vector2 touchPoint) {
        return guiRenderer.buttonConfigPressed(touchPoint);
    }

    public boolean buttonInventoryPressed(Vector2 touchPoint) {
        return guiRenderer.buttonInventoryPressed(touchPoint);
    }

    public boolean isMusicOn() {
        return game.isMusicOn();
    }

    public boolean isSfxEnabled() {
        return game.isSfxEnabled();
    }

    public boolean isTilesetGraphical() {
        return game.isTilesetGraphical();
    }

    public boolean shouldShowPad() {
        return game.shouldShowPad();
    }

    public void toggleMusic() {
        bgMusic.toggle();
        game.setIsMusicOn(bgMusic.isPlaying());
    }

    public void toggleSfx() {
        sfx.toggle();
        game.setIsSfxOn(sfx.isEnabled());
    }

    public void toggleTileset() {
        renderer.switchAtlas();
        guiRenderer.switchAtlas();
        game.setIsTilesetGraphical(renderer.isAtlasGraphical());
    }

    public void togglePad() {
        game.setShouldShodPad(!shouldShowPad());
    }

    public void abandonQuest() {
        restartGame();
    }

    public long getSeed() {
        return seed;
    }

    public void togglePadPos() {
        game.setRightPad(!isPadAtRight());
    }

    public boolean isPadAtRight() {
        return game.isPadAtRight();
    }
}
