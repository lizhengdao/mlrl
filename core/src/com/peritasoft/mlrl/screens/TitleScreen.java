/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.easing.EaseInBack;
import com.peritasoft.mlrl.easing.EaseOutBack;
import com.peritasoft.mlrl.easing.Easing;
import com.peritasoft.mlrl.render.TitleBackground;
import com.peritasoft.mlrl.ui.Button;

import java.util.HashMap;
import java.util.Map;

public class TitleScreen extends MlrlScreenAdapter {
    private final MyLittleRogueLike game;
    private final Button newGameButton;
    private final Button resumeGameButton;
    private final Button endlessGameButton;
    private final Button scoresButton;
    private final TextureAtlas atlasInterface;
    private final ShapeRenderer shapes;
    private TitleBackground background;
    private final Map<Button, Easing> buttonEasings;
    private Runnable fadingTo;

    public TitleScreen(MyLittleRogueLike game) {
        this(game, new TitleBackground());
    }

    public TitleScreen(MyLittleRogueLike game, TitleBackground background) {
        this.game = game;
        this.background = background;
        shapes = new ShapeRenderer();
        atlasInterface = new TextureAtlas("td_interface.atlas");
        final NinePatch emptyButton = new NinePatch(atlasInterface.findRegion("buttonEmpty"), 8, 8, 4, 4);
        final NinePatch emptyButtonPressed = new NinePatch(atlasInterface.findRegion("buttonEmptyPressed"), 8, 8, 4, 4);

        final float buttonMargin = 35f;
        final float buttonX = 16f;
        final float buttonY = MyLittleRogueLike.SCREENHEIGHT / 2f + 5f;
        final float buttonWidth = 128f;
        final float buttonOffset = -buttonWidth - 10f;
        final float buttonDelay = 0.1f;

        background.setMlrlTitle();
        buttonEasings = new HashMap<>();
        resumeGameButton = new Button(buttonX, buttonY - buttonMargin * 0, buttonWidth,
                emptyButton, emptyButtonPressed,
                "Resume Game");
        buttonEasings.put(resumeGameButton, new EaseOutBack(buttonOffset, 0, buttonDelay * 0));

        newGameButton = new Button(buttonX, buttonY - buttonMargin * 1, buttonWidth,
                emptyButton, emptyButtonPressed,
                "New Game");
        buttonEasings.put(newGameButton, new EaseOutBack(buttonOffset, 0, buttonDelay * 1));

        endlessGameButton = new Button(buttonX, buttonY - buttonMargin * 2, buttonWidth,
                emptyButton, emptyButtonPressed,
                "Endless Run");
        buttonEasings.put(endlessGameButton, new EaseOutBack(buttonOffset, 0, buttonDelay * 2));

        scoresButton = new Button(buttonX, buttonY - buttonMargin * 3, buttonWidth,
                emptyButton, emptyButtonPressed,
                "Scores");
        buttonEasings.put(scoresButton, new EaseOutBack(buttonOffset, 0, buttonDelay * 3));
    }

    @Override
    public void render(float delta) {
        update(delta);
        if (isFading() && isFadingComplete()) {
            fadingTo.run();
        } else {
            draw(game.getBatch());
        }
    }

    private void update(float delta) {
        background.update(delta);
        for (Easing easing : buttonEasings.values()) {
            easing.update(delta);
        }
    }

    private void draw(Batch batch) {
        background.clear();
        batch.begin();
        background.draw(batch);
        drawButton(batch, resumeGameButton, game.canResumeGame());
        drawButton(batch, newGameButton, true);
        drawButton(batch, endlessGameButton, game.isEndlessUnlocked());
        drawButton(batch, scoresButton, true);
        batch.end();

        Gdx.gl.glEnable(GL20.GL_BLEND);
        shapes.begin(ShapeRenderer.ShapeType.Filled);
        shapes.setProjectionMatrix(batch.getProjectionMatrix());
        if (!game.canResumeGame()) fadeButton(resumeGameButton);
        if(!game.isEndlessUnlocked()) fadeButton(endlessGameButton);
        shapes.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    private void drawButton(Batch batch, Button button, boolean enabled) {
        final BitmapFont font = game.getSmallFont();
        if (enabled) font.setColor(1f, 1f, 1f, 1f);
        else font.setColor(0.1f, 0.1f, 0.1f, 1f);

        Easing easing = buttonEasings.get(button);
        final boolean setYOffset = isFading();
        button.setOffset(setYOffset ? 0 : easing.getValue(), setYOffset ? easing.getValue() : 0);
        button.draw(batch, font);

        if (!enabled) font.setColor(1f, 1f, 1f, 1f);
    }

    private void fadeButton(Button button) {
        shapes.setColor(1f, 1f, 1f, 0.2f);
        final Rectangle rect = button.getRect();
        final Vector2 offset = button.getOffset();
        shapes.rect(rect.x + offset.x, rect.y + offset.y, rect.width, rect.height);
    }

    @Override
    public boolean touchDown(Vector2 touchPoint, int pointer, int button) {
        if (isFading()) {
            return false;
        }
        if (game.canResumeGame() && resumeGameButton.pressed(touchPoint)) {
            fadeTo(new Runnable() {
                @Override
                public void run() {
                    game.resumeGame();
                    dispose();
                }
            });
            return true;
        }
        if (newGameButton.pressed(touchPoint)) {
            fadeTo(new Runnable() {
                @Override
                public void run() {
                    game.setScreen(new SelectScreen(game, background, false));
                    background = null; // ownership transfer to SelectScreen.
                    dispose();
                }
            });
            return true;
        }
        if (endlessGameButton.pressed(touchPoint)) {
            fadeTo(new Runnable() {
                @Override
                public void run() {
                    game.setScreen(new SelectScreen(game, background, true));
                    background = null; // ownership transfer to SelectScreen.
                    dispose();
                }
            });
            return true;
        }
        if (scoresButton.pressed(touchPoint)) {
            fadeTo(new Runnable() {
                @Override
                public void run() {
                    game.setScreen(new ScoreScreen(game, background));
                    background = null; // ownership transfer to ScoreScreen
                    dispose();
                }
            });
            return true;
        }
        return false;
    }

    private void fadeTo(Runnable runnable) {
        if (isFading()) return;
        background.hideTitle();
        buttonEasings.clear();
        final float endButtonOffset = -MyLittleRogueLike.SCREENHEIGHT;
        final float buttonDelay = 0.1f;
        buttonEasings.put(resumeGameButton, new EaseInBack(0f, endButtonOffset, buttonDelay * 3));
        buttonEasings.put(newGameButton, new EaseInBack(0f, endButtonOffset, buttonDelay * 2));
        buttonEasings.put(endlessGameButton, new EaseInBack(0f, endButtonOffset, buttonDelay * 1));
        buttonEasings.put(scoresButton, new EaseInBack(0f, endButtonOffset, buttonDelay * 0));
        fadingTo = runnable;
    }

    private boolean isFading() {
        return fadingTo != null;
    }

    private boolean isFadingComplete() {
        for (Easing easing : buttonEasings.values()) {
            if (!easing.isDone()) {
                return false;
            }
        }
        return true;
    }


    @Override
    public void dispose() {
        atlasInterface.dispose();
        shapes.dispose();
        if (background != null) {
            background.dispose();
        }
    }
}
